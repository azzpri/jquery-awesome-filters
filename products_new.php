<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if( defined('DOING_AJAX') && DOING_AJAX ) {
	add_action('wp_ajax_nopriv_getServers', 'getServers_callback');
	add_action('wp_ajax_getServers', 'getServers_callback');

	add_action('wp_ajax_nopriv_getSuggestions', 'getSuggestions_callback');
	add_action('wp_ajax_getSuggestions', 'getSuggestions_callback');
}

function sql_execute($db, $query, $params) {
	$stmt = $db->prepare($query);
	foreach ($params as $idx => $val) {
	    $stmt->bindValue($idx+1, $val);
	}
	return $stmt->execute();
}

function arr_get($array, $key, $default = null) {
	return isset($array[$key]) ? $array[$key] : $default;
}

function arr_int_get($array, $key, $default = null) {
	return (int)abs(arr_get($array, $key, $default));
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function get_currency() {
	if(arr_get($_SERVER, 'HTTP_COOKIE') && endsWith($_SERVER['HTTP_HOST'], '.hostboss.org') && $ch = curl_init()) {
		$strCookie = $_SERVER['HTTP_COOKIE'];

		curl_setopt($ch, CURLOPT_URL, 'http://whmcs/members/modules/addons/responsio/feeds.php?get=currency');
		curl_setopt($ch, CURLOPT_COOKIE, $strCookie);

		$pass_headers = array(
		    'Host' => 'HTTP_HOST',
		    'Accept' => 'HTTP_ACCEPT',
		    'Accept-Charset' => 'HTTP_ACCEPT_CHARSET',
		    'Accept-Encoding' => 'HTTP_ACCEPT_ENCODING',
		    'Accept-Language' => 'HTTP_ACCEPT_LANGUAGE',
#		    'Connection' => 'HTTP_CONNECTION',
		    'Referer' => 'HTTP_REFERER',
		    'X-Real-IP' => 'REMOTE_ADDR',
		    'X-Forwarded-For' => 'REMOTE_ADDR',
		    'User-Agent' => 'HTTP_USER_AGENT',
		);

		$curl_request_headers = array();
		foreach($pass_headers as $header_key => $server_key) {
		    $val = arr_get($_SERVER, $server_key);
		    if ($val) {
			$curl_request_headers[] = $header_key.': '.$_SERVER[$server_key];
		    }
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_request_headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$contents = curl_exec($ch);
		curl_close($ch);

		if (preg_match_all("/document.write\('(EUR|USD)'\)/i", $contents, $links, PREG_PATTERN_ORDER)) {
		    return $links[1][0];
		}
	}
	return 'USD';
}

function getSuggestions($param) {
	$db = new SQLite3(dirname(__FILE__) . '/servers.db');

	$res = sql_execute($db,
		"SELECT keyword
		 FROM search
		 WHERE keyword LIKE ?
		 ORDER BY keyword
		", [$param.'%']
		);

	$result = array();
	while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
		$result[] = $row['keyword'];
	}

	return Array('suggestions' => $result);
}

function getServers($request_params =Array()) {

	// get currency
    $currency = get_currency();

	$rules = Array(
		'countries' => Array('array', 'country'),
	    'cpu_cores' => Array('array', 'cpu_cores'),
	    'cpu_procs' => Array('array', 'cpu_procs'),
	    // 'cpu_brand' => Array('map', 'cpu_brand'),
	    // 'cpu_fam1' => Array('map', 'cpu_fam1'),
	    'ram_ecc' => Array('int', 'ram_ecc'),
	    'ram_type' => Array('map', 'ram_type'),
	    'ram_rate' => Array('map', 'ram_rate'),
	    'ram_size' => Array('range', 'ram_size'),
	    'hdd_num' => Array('range', 'hdd_num'),
	    'hdd_type' => Array('map', 'hdd_type'),
	    'hdd_size' => Array('range', 'hdd_size'),
	    'bw_num' => Array('range', 'bw_num'),
	    'bw_uplink' => Array('map', 'bw_uplink'),
	    'server_cost' => Array('range', 'server_cost_' . $currency),
	    'address' => Array('range', 'address'),
	    'deployment' => Array('range', 'deployment'),
	    'table_search' => Array('search', NULL),
	);

    $sort_rules = Array(
    	'ip' => 'address',
    	'cpu' => 'cpu_brand,cpu_fam,cpu_model',
    	'cpu_score' => 'cpu_mark',
    	'ram' => 'ram_size',
    	'hdd' => 'hdd_size',
    	// 'hdd' => '(hdd_num*hdd_size)',
    	'traffic' => 'bw_num',
    	'price' => 'server_cost_' . $currency,
    	'undefined' => 'cpu_mark',
    );

    $sort_rules_order = Array(
    	'up' => 'ASC',
    	'down' => 'DESC',
    );

	// get filters map
	$string = file_get_contents(dirname(__FILE__) . '/assets/js/filters_map.json');
	$filters_map = json_decode($string, true);

	$db = new SQLite3(dirname(__FILE__) . '/servers.db');

	$query_where = '1=1';
	$query_params = Array();

	# Main parser
	foreach ($request_params as $key => $value) {
	    if (array_key_exists($key, $rules)) {

			$where = Array();
			$params = Array();

			list($type, $field) = $rules[$key];

			switch ($type) {

				case 'int':
					$where[] = $field . '=?';
					$params[] = (int)$value;
					break;

			    case 'range':
					$params = explode(':', $value, 2);
					$where[] = $field . ' BETWEEN ? AND ?';
					break;

			    case 'array':
					$params = explode(',', $value);
					$where[] = $field . ' IN (' . str_repeat('?,', count($params) - 1) . '?' . ')';

					# Handler for 'more' in cpu_cores
					if ($key == 'cpu_cores' && in_array('more', $params)) {
						$where[] = $field . '>?';
						$params[] = 10;
					}
					break;

			    case 'map':
					foreach (explode(',', $value) as $idx) {
					    if (isset($filters_map[$key][$idx])) {
							$params[] = $filters_map[$key][$idx];
					    }
					}
					if (count($params)) {
					    $where[] = $field . ' IN (' . str_repeat('?,', count($params) - 1) . '?' . ')';
					}
					break;

				case 'search':
					$params_tmp = explode(',', $value);

					$res = sql_execute($db,
						"SELECT section, GROUP_CONCAT(ids) ids
						 FROM search
						 WHERE keyword IN (" . str_repeat('?,', count($params_tmp) - 1) . '?' . ")
						 GROUP BY section
						 ", $params_tmp
					);
					
					$result = array();
					while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
						$params_tmp = explode(',', $row['ids']);
						$params = array_merge($params, $params_tmp);
						$where[] = strtolower($row['section']) . '.`id` IN (' . str_repeat('?,', count($params_tmp) - 1) . '?' . ')';
					}

					// Hack for all search args with AND rule
					if ($params) {
						$where = Array(implode(' AND ', $where));
					}
			}

			if ($where) {
				$where = ' AND ('.implode(' OR ', $where).') ';

				if (substr_count($where, '?') == count($params)) {
				    $query_where .= $where;
				    $query_params = array_merge($query_params, $params);
				}
			}
	    }
	}

	$rules_map = Array(
		'os' => Array(
			'table' => 'os',
			'dep_field' => 'os',
			'val_field' => 'ver',
			),
		'cpu_brand' => Array(
			'table' => 'cpu',
			'dep_field' => 'cpu_fam',
			'val_field' => 'cpu_fam',
			'excl_field' => 'cpu_family_excl',
			),
	);

	# Nested fields like cpu/os
	foreach ($rules_map as $key => $info) {

		if (array_key_exists($key, $filters_map)) {

			$count = count($filters_map[$key]);

			$values_map = Array();

			foreach(range(1, $count) as $idx) {
				$field = $info['dep_field'].$idx;

				if (array_key_exists($field, $request_params)) {

					foreach (explode(',', $request_params[$field]) as $idx) {
					    if (isset($filters_map[$field][$idx])) {
					    	$values_map[$info['val_field']][] = $filters_map[$field][$idx]; ## OS ????
					    	// $values_map[$info['dep_field']][] = $filters_map[$field][$idx];
					    }
					}

				}
			}

			if (array_key_exists($key, $request_params)) {
				foreach (explode(',', $request_params[$key]) as $idx) {

					if (array_key_exists($info['dep_field'].$idx, $request_params)) {
						continue;
					}

					if (isset($filters_map[$key][$idx])) {
				    	$values_map[$key][] = $filters_map[$key][$idx];
				    }

				}
			}

			$where = Array();
			$params = Array();

			$is_excl = array_key_exists('excl_field', $info) ? arr_int_get($request_params, $info['excl_field'], 0) : 0;

			foreach($values_map as $key => $values) {

				$need_NOT = '';
				if (startsWith($key, $info['dep_field']) && $is_excl) {
					$need_NOT = ' NOT ';
				}

				$where[] = $info['table'] .'.'. $key . $need_NOT . ' IN (' . str_repeat('?,', count($values) - 1) . '?' . ')';
				$params = array_merge($params, $values);
			}

			if ($where) {
				$where = ' AND ('.implode(' OR ', $where).') ';

				if (substr_count($where, '?') == count($params)) {
				    $query_where .= $where;
				    $query_params = array_merge($query_params, $params);
				}
			}

		}
	}

	// echo $query_where . "\n";
	// var_dump($query_params);
	// exit();

	$n = arr_int_get($request_params, 'show', 10);
	$page = arr_int_get($request_params, 'page', 1);
	$query_orderby = 'p.id ASC';

	# Main query build
	$select = "
	FROM products p
	LEFT JOIN os ON os.product_id=p.id
	LEFT JOIN cpu ON cpu.product_id=p.id
	LEFT JOIN ram ON ram.product_id=p.id
	LEFT JOIN hdd ON hdd.product_id=p.id
	LEFT JOIN country ON country.product_id=p.id
	WHERE " . $query_where;


	# Pagination
	$res = sql_execute($db,
		'SELECT COUNT(DISTINCT p.id) '. $select,
		$query_params
	);


	$rows_count = $res->fetchArray(SQLITE3_NUM)[0];
	$total_pages = $rows_count ? ceil($rows_count/$n) : 1;

	if ($page > $total_pages)
		$page = $total_pages;
	else if ($page <= 0)
        $page = 1;


   	# Order by build

    $sort = explode(',', arr_get($request_params, 'sort'));
    if(count($sort) == 2) {
    	list($field, $order) = $sort;

    	if($db_field = arr_get($sort_rules, $field)) {
    		if ($db_order = arr_get($sort_rules_order, $order)) {
    			$query_orderby = implode(' '.$db_order.',', explode(',', $db_field)) . ' '.$db_order;
    		}
    	}
    }

    # Querying

    $res = sql_execute($db,
    	"SELECT
    		pid,
    		brands,
	    	cpu_procs,
			cpu_brand,
			cpu_fam,
			cpu_model,
			cpu_freq,
			cpu_cores,
			cpu_threads,
			(cpu_mark * cpu_procs) cpu_mark,

			ram_size,
			ram_type,
			ram_ecc,
			ram_rate,

			GROUP_CONCAT(
				DISTINCT printf('%ix %i %s', hdd.hdd_num, hdd.hdd_size, hdd.hdd_type)
				-- hdd.num || hdd.size || hdd.type
			) hdd,

			bw_num,
			bw_uplink,

			address,
			address2,

			GROUP_CONCAT(
				DISTINCT country
			) countries,

			server_cost_" . $currency . " AS server_cost,

			gid
    	 ". $select ." AND p.id>? GROUP BY p.id ORDER BY " . $query_orderby . " LIMIT ?",
    	array_merge($query_params, array(($page-1)*$n, $n))
    );

    # Output results
	$result = array();
	while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
	    #$result[] = $row;
		//BRAND
		$ans = array( '' );
		#$ans = array( isset( $row['brands'] ) ? $row['brands'] : '' );

		// HOT DEAL
		$ans[] = '';

		// CPU
		$cpu_string = '';
		if( isset( $row['cpu_procs'] ) && $row['cpu_procs'] > 1 ) $cpu_string .= $row['cpu_procs'] . "x "; // 2x
		if( isset( $row['cpu_brand'] ) ) $cpu_string .= $row['cpu_brand'] . " "; // Intel
		if( isset( $row['cpu_fam'] ) ) $cpu_string .= $row['cpu_fam'] . " "; // Xeon
		if( isset( $row['cpu_model'] ) ) $cpu_string .= $row['cpu_model'] . " "; // Xeon
		if( isset( $row['cpu_freq'] ) ) $cpu_string .= '@'.$row['cpu_freq'] . "GHz<br/>"; // Xeon
		if( isset( $row['cpu_cores'] ) ) $cpu_string .= $row['cpu_cores'] . " Cores/"; // 8 Cores
		if( isset( $row['cpu_threads'] ) ) $cpu_string .= $row['cpu_threads'] . " Threads"; // 8 Threads
		$ans[] = $cpu_string;

		// CPU SCORE
		$ans[] = isset( $row['cpu_mark'] ) ? $row['cpu_mark'] : '';

		// RAM

		$ram_array = array(
			($row['ram_size'] ? $row['ram_size'] ."GB " : '').($row['ram_ecc'] ? "ECC" : ''),
			($row['ram_type'] ? $row['ram_type'] ." " : '').($row['ram_rate'] ? '@'.$row['ram_rate'] : '')
		);

		// $ram_string = '<p title="test">';
		// $ram_string .= isset( $row['ram_size'] ) ? $row['ram_size'] ."GB " : '';
		// $ram_string .= isset( $row['ram_type'] ) ? $row['ram_type'] ." " : '';
		// $ram_string .= isset( $row['ram_ecc'] ) ? "ECC" : '';
		// $ram_string .= $row['ram_rate'] ? '<br/>@'.$row['ram_rate'] : '';
		// $ram_string .= '</p>' ;
		$ans[] = $ram_array;

		// HDD
		#$hdd_string = isset( $row['hdd'] ) && $row['hdd']>1 ? $row['hdd'] ."x" : '';
		$hdd_string = explode(',', $row['hdd']);

		array_walk($hdd_string, function(&$value, $key, $param) {
			if(startsWith($value, $param)) {
				$value = substr($value, strlen($param));
			}
    	}, '1x ');
		// $hdd_string = isset( $row['hdd_num'] ) && $row['hdd_num']>1 ? $row['hdd_num'] ."x" : '';
		// $hdd_string .= isset( $row['hdd_size'] ) ? $row['hdd_size'] ."GB" : '';
		// $hdd_string .= isset( $row['hdd_type'] ) ? " ".$row['hdd_type'] : '';
		#$ans[] = array($hdd_string);
		$ans[] = $hdd_string;

		// TRAFFIC
		if ($row['bw_num'] != 'Unlimited') {
			$traffic_string = isset( $row['bw_num'] ) ? $row['bw_num'] ." Mbps <br/>" : '';
		}
		else {
			$traffic_string = isset( $row['bw_num'] ) ? $row['bw_num'] ."<br/>" : '';
		}
		$traffic_string .= isset( $row['bw_uplink'] ) ? $row['bw_uplink'] : '';
		$ans[] = $traffic_string;

		//IP
		$ans[] = array(
			// isset( $row['address'] ) ? $row['address'] : '' ,
			isset( $row['address2'] ) ? $row['address2'] ." IP" : ''
		);

		//LOCATION
		$ans[] = isset( $row['countries'] ) ? $row['countries'] : '';

		//PRICE
		$ans[] = isset( $row['server_cost'] ) ? $row['server_cost'] : '';

		//Url
		$ans[] = isset( $row['gid'] ) ? '/members/cart.php?a=add&pid=' . $row['pid'] : '';

		//cpu score url
		$ans[] = isset( $row['gid'] ) ? 'http://cpubenchmark.net/' : '';

		// //cpu url
		// $ans[] = isset( $row['gid'] ) ? '/cpu/whmcs/cart.php?a=add&pid=' . $row['pid'] : '';

		$result['items'][] = $ans;
	    #var_dump($row['name']);
	}

	$result['page'] = $page;
	$result['currency'] = $currency;
	$result['item_count'] = $rows_count;
	$result['page_count'] = $total_pages;

	return $result;
}

function getServers_callback () {
	// parse incoming data
	$data = file_get_contents("php://input");
	$data = strstr($data, '#');
	$data = str_replace( "#", "&", $data);
	parse_str($data, $result);

	print_r(json_encode( getServers($result) ));

	wp_die();
}

function getSuggestions_callback() {
	print_r(json_encode( getSuggestions($_GET['query']) ));

	wp_die();
}

?>