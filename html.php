<!-- <script>
    var initial_data = [];
</script> -->
<div class="bg_parallax  vc_custom_1422961126796">
    <div class="container">
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner vc_custom_1464015338332">
                    <div class="wpb_wrapper">
                        <div class="ts-section-title  text-center" style="color:#737373">
                            <p>Choose <b>Dedicated Servers</b> of Your needs out of 100,343 offers in 32 countries!</p>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid"></div>
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <!-- header Start -->
                        <div class="awesome_header">                            
                            <div class="header__bottom-panel __indent-none">
                                <div class="header__bottom-panel__inner">
                                    <ul class="upper-filter">                                        
                                        <li class="upper-filter__item">
                                            <div class="upper-filter__top-panel">
                                                <span class="upper-filter__title">Country</span>
                                                <div class="question">
                                                    <div class="question__icon">?</div>
                                                    <div class="question__description">
                                                        <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                        <div class="question__description__corm">&nbsp;</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upper-filter__bottom-panel">
                                                <div class="upper-filter__select">
                                                    <input type="text" value="0 Selected" data-dropdown="true" data-filter="countries"/>
                                                    <div class="upper-filter__dropdown-menu __city" data-dropdown-target="true">
                                                        <div class="upper-filter__dropdown-menu__inner">
                                                            <ul>
                                                                <li>
                                                                    <div class="upper-filter__dropdown-menu__panel">
                                                                        <label for="north-america">North America</label>
                                                                        <input id="north-america" type="checkbox" name="countries" autocomplete="false"/>
                                                                    </div>    
                                                                    <ul>
                                                                        <li>
                                                                            <input id="unites-states" type="checkbox" name="countries" value="us" autocomplete="off"/>
                                                                            <label for="unites-states">Unites States</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="canada" type="checkbox" name="countries" value="ca" autocomplete="off"/>
                                                                            <label for="canada">Canada</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="mexico" type="checkbox" name="countries" value="mx" autocomplete="off"/>
                                                                            <label for="mexico">Mexico</label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <div class="upper-filter__dropdown-menu__panel">
                                                                        <label for="south-america">South America</label>
                                                                        <input id="south-america" type="checkbox" name="countries"/>
                                                                    </div>    
                                                                    <ul>
                                                                        <li>
                                                                            <input id="argentina" type="checkbox" name="countries"value="ag" />
                                                                            <label for="argentina">Argentina</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="brazil" type="checkbox" name="countries" value="br"/>
                                                                            <label for="brazil">Brazil</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="chile" type="checkbox" name="countries" value="cl"/>
                                                                            <label for="chile">Chile</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="colombia" type="checkbox" name="countries"value="co"/>
                                                                            <label for="colombia">Colombia</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="venezuela" type="checkbox" name="countries"value="ve"/>
                                                                            <label for="venezuela">Venezuela</label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <div class="upper-filter__dropdown-menu__panel">
                                                                        <label for="europe">Europe</label>
                                                                        <input id="europe" type="checkbox" />
                                                                    </div>    
                                                                    <ul>
                                                                        <li>
                                                                            <input id="germany" type="checkbox" name="countries" value="de"/>
                                                                            <label for="germany">Germany</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="spain" type="checkbox" name="countries" value="es"/>
                                                                            <label for="spain">Spain</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="france" type="checkbox" name="countries" value="fr"/>
                                                                            <label for="france">France</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="italy" type="checkbox" name="countries" value="it"/>
                                                                            <label for="italy">Italy</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="poland" type="checkbox" name="countries" value="pl"/>
                                                                            <label for="poland">Poland</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="russia" type="checkbox" name="countries" value="ru"/>
                                                                            <label for="russia">Russia</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="turkey" type="checkbox" name="countries" value="tr"/>
                                                                            <label for="turkey">Turkey</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="united-kingdom" type="checkbox" name="countries" value="gb"/>
                                                                            <label for="united-kingdom">United Kingdom</label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <div class="upper-filter__dropdown-menu__panel">
                                                                        <label for="asia">Asia</label>
                                                                        <input id="asia" type="checkbox" name="countries"/>
                                                                    </div>    
                                                                    <ul>
                                                                        <li>
                                                                            <input id="Japan" type="checkbox" name="countries" value="jp"/>
                                                                            <label for="Japan">Japan</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="china" type="checkbox" name="countries" value="cn"/>
                                                                            <label for="china">China</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="india" type="checkbox" name="countries" value="in"/>
                                                                            <label for="india">India</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="korea" type="checkbox" name="countries" value="kr"/>
                                                                            <label for="korea">Korea</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="philippines" type="checkbox" name="countries" value="ph"/>
                                                                            <label for="philippines">Philippines</label>
                                                                        </li>
                                                                    </ul>
                                                                </li> 
                                                            </ul>
                                                        </div>    
                                                    </div>
                                                </div>
                                                <a href="#" class="upper-filter__delecte">
                                                    <span>delecte</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="upper-filter__item">
                                            <div class="upper-filter__top-panel">
                                                <span class="upper-filter__title">Server cost</span>
                                                <div class="question">
                                                    <div class="question__icon">?</div>
                                                    <div class="question__description">
                                                        <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                        <div class="question__description__corm">&nbsp;</div>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="upper-filter__bottom-panel">
                                                <div class="upper-filter__select server_cost_container">
                                                    <input type="text" value="0$ - 500$" data-dropdown="true" data-filter="server_cost"/>
                                                    <div class="upper-filter__dropdown-menu" data-dropdown-target="true">
                                                        <div class="upper-filter__dropdown-menu__inner">
                                                            <div class="global-slider">
                                                                <span data-slider="cost-min"></span>
                                                                <div data-slider="true" data-slider-name="server_cost" data-slider-range="0,500" data-slider-value="0, 500" data-slider-target="cost-min,cost-max" data-slider-type="$"></div>
                                                                <span data-slider="cost-max"></span>
                                                            </div>    
                                                        </div>
                                                    </div>    
                                                </div>
                                                <a href="#" class="upper-filter__delecte">
                                                    <span>delecte</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="upper-filter__item">
                                            <div class="upper-filter__top-panel">
                                                <span class="upper-filter__title">CPU & Cores</span>
                                                <div class="question">
                                                    <div class="question__icon">?</div>
                                                    <div class="question__description">
                                                        <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                        <div class="question__description__corm">&nbsp;</div>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="upper-filter__bottom-panel">
                                                <div class="upper-filter__select">
                                                    <input type="text" value="2x8 Cores" data-dropdown="true" data-filter="cpu_procs"/>
                                                    <div class="upper-filter__dropdown-menu __cores" data-dropdown-target="true">
                                                        <div class="upper-filter__dropdown-menu__inner">
                                                            <ul>
                                                                <li>   
                                                                    <ul>
                                                                        <li>
                                                                            <input id="single-processor" type="checkbox" name="cpu_procs" value="1" autocomplete="off"/>
                                                                            <label for="single-processor">Single Processor</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="two-processors" type="checkbox" name="cpu_procs" value="2" autocomplete="off"/>
                                                                            <label for="two-processors">Two Processors</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="four-processors" type="checkbox" name="cpu_procs" value="4" autocomplete="off"/>
                                                                            <label for="four-processors">Four Processors</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="more" type="checkbox" name="cpu_procs" value="more"/>
                                                                            <label for="more">more Processors</label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>   
                                                                    <ul>
                                                                        <li>
                                                                            <input id="1-core" type="checkbox" name="cpu_cores" value="1"/>
                                                                            <label for="1-core">1 Core</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="2-cores" type="checkbox" name="cpu_cores" value="2"/>
                                                                            <label for="2-cores">2 Cores</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="4-cores" type="checkbox" name="cpu_cores" value="4"/>
                                                                            <label for="4-cores">4 Cores</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="6-cores" type="checkbox" name="cpu_cores" value="6"/>
                                                                            <label for="6-cores">6 Cores</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="8-cores" type="checkbox" name="cpu_cores" value="8"/>
                                                                            <label for="8-cores">8 Cores</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="10-cores" type="checkbox" name="cpu_cores" value="10"/>
                                                                            <label for="10-cores">10 Cores</label>
                                                                        </li>
                                                                        <li>
                                                                            <input id="more2" type="checkbox" name="cpu_cores" value="more"/>
                                                                            <label for="more2">more Cores</label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <a href="#" class="upper-filter__delecte">
                                                    <span>delecte</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="upper-filter__item">
                                            <div class="upper-filter__top-panel">
                                                <span class="upper-filter__title">RAM</span>
                                                <div class="question">
                                                    <div class="question__icon">?</div>
                                                    <div class="question__description">
                                                        <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                        <div class="question__description__corm">&nbsp;</div>
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="upper-filter__bottom-panel">
                                                <div class="upper-filter__select ram_size_container">
                                                    <input type="text" value="0GB - 10GB" data-dropdown="true" data-filter="ram_size"/>
                                                    <div class="upper-filter__dropdown-menu" data-dropdown-target="true">
                                                        <div class="upper-filter__dropdown-menu__inner">
                                                            <div class="global-slider">
                                                                <span data-slider="ram-min"></span>
                                                                <div data-slider="true" data-slider-name="ram_size" data-slider-range="0,10" data-slider-target="ram-min,ram-max" data-slider-type="GB"></div>
                                                                <span data-slider="ram-max"></span>
                                                            </div>    
                                                        </div>    
                                                    </div>  
                                                </div>
                                                <a href="#" class="upper-filter__delecte">
                                                    <span>delecte</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="upper-filter__item __advanced">
                                            <ul class="upper-filter__list">
                                                <li class="upper-filter__list__item">
                                                    <div class="upper-filter__top-panel">
                                                        <span class="upper-filter__title">HDD Type</span>
                                                        <div class="question">
                                                            <div class="question__icon">?</div>
                                                            <div class="question__description">
                                                                <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                                <div class="question__description__corm">&nbsp;</div>
                                                            </div>
                                                        </div>    
                                                    </div>
                                                    <div class="upper-filter__bottom-panel">
                                                        <div class="upper-filter__select">
                                                            <input type="text" value="&nbsp; -" data-dropdown="true" data-filter="hdd_type"/>
                                                            <div class="upper-filter__dropdown-menu __type" data-dropdown-target="true">
                                                                <div class="upper-filter__dropdown-menu__inner">
                                                                    <ul>
                                                                        <li>   
                                                                            <ul>
                                                                                <li>
                                                                                    <input id="sas" type="checkbox" name="hdd_type" value="1"/>
                                                                                    <label for="sas">SAS</label>
                                                                                </li>
                                                                                <li>
                                                                                    <input id="sata" type="checkbox" name="hdd_type" value="2"/>
                                                                                    <label for="sata">SATA</label>                                                                                
                                                                                </li>
                                                                                <li>
                                                                                    <input id="ssd" type="checkbox" name="hdd_type" value="3"/>
                                                                                    <label for="ssd">SSD</label>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>    
                                                            </div>
                                                        </div>
                                                        <a href="#" class="upper-filter__delecte">
                                                            <span>delecte</span>
                                                        </a>
                                                    </div>
                                                </li>    
                                                <li class="upper-filter__list__item">
                                                    <div class="upper-filter__top-panel">
                                                        <span class="upper-filter__title">Storage</span>
                                                        <div class="question">
                                                            <div class="question__icon">?</div>
                                                            <div class="question__description">
                                                                <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                                <div class="question__description__corm">&nbsp;</div>
                                                            </div>
                                                        </div>    
                                                    </div>
                                                    <div class="upper-filter__bottom-panel">
                                                        <div class="upper-filter__select hdd_size_container">
                                                            <input type="text" value="0GB - 3TB" data-dropdown="true" data-filter="hdd_size"/>
                                                            <div class="upper-filter__dropdown-menu" data-dropdown-target="true">
                                                                <div class="upper-filter__dropdown-menu__inner">
                                                                    <div class="global-slider">
                                                                        <span data-slider="hdd-min"></span>
                                                                        <div data-slider="true" data-slider-name="hdd_size" data-slider-range="0,3000" data-slider-target="hdd-min,hdd-max" data-slider-type="MB"></div>
                                                                        <span data-slider="hdd-max"></span>
                                                                    </div>    
                                                                </div>    
                                                            </div>  
                                                        </div>
                                                        <a href="#" class="upper-filter__delecte">
                                                            <span>delecte</span>
                                                        </a>
                                                    </div>
                                                </li>    
                                            </ul>    
                                        </li>
                                        <li class="upper-filter__item user-filters">
                                            <div class="upper-filter__top-panel">
                                                <div class="upper-filter__left">
                                                    <span class="upper-filter__title"></span>
                                                    <div class="question">
                                                        <div class="question__icon">?</div>
                                                        <div class="question__description">
                                                            <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                            <div class="question__description__corm">&nbsp;</div>
                                                        </div>
                                                    </div>    
                                                </div>    
                                                <a href="#" class="user-filters__add">add new</a>
                                                <a href="#" class="user-filters__delete">delete</a>
                                            </div>
                                            <div class="upper-filter__select">
                                                <select class="user-filters__select">
                                                    <option>Select Saved Settings</option>
                                                    <option>Select Saved Settings</option>
                                                    <option>Select Saved Settings</option>
                                                </select>
                                            </div >
                                            <div class="user-filters__new" >
                                                <div class="upper-filter__left">
                                                    <span class="upper-filter__title">New filter</span>
                                                </div>
                                                <a href="#" class="user-filters__save">save</a>
                                                <a href="#" class="user-filters__cancel ">cancel</a>
                                                <input placeholder="filter name" value="filter name">
                                                
                                            </div>
                                        </li>
                                    </ul>                                    
                                    <nav class="under-navi">
                                        <ul>                                            
                                            <li class="under-navi__item">
                                                <a href="#">CPU</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">RAM</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">HDD & Storage</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">RAID</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">IP Addresses</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">Bandwidth</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">OS</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">Deployment Time</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">Add ons</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">Brand</a>
                                            </li>
                                            <li class="under-navi__item">
                                                <a href="#">Allowed Content</a>
                                            </li>                              
                                        </ul>                                    
                                        <span class="upper-filter__button">More Filters</span>                                        
                                    </nav>
                                    <div class="under-navi__sub-list">
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item cpu_brand_li">
                                                <span>Manufacturer:</span>
                                                <dl>                                                    
                                                    <!--<dd>
                                                        <input id="intel" type="checkbox" radio-filter-name="cpu_brand" value="1" />
                                                        <label for="intel" >Intel</label>
                                                    </dd>
                                                    <dd>
                                                        <input id="amd" type="checkbox" radio-filter-name="cpu_brand"  value="2"/>
                                                        <label for="amd" >AMD</label>
                                                    </dd>-->
                                                </dl>   
                                            </li>
                                            <li class="under-navi__sub-list__item cpu_fam_li">
                                                <!--<span class="cpu_fam1_container"> CPU Family:</span>
                                                <span class="cpu_fam2_container"> CPU Family:</span>-->
                                                <dl>                                                    
                                                   <!-- <dd class="cpu_fam1_container">
                                                        <div class="under-navi__select">
                                                            <input type="text" value="2 selected" data-dropdown="true" data-filter="cpu_fam1"/>
                                                            <div class="under-navi__select__dropdown cpu_fam1" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <div class="under-navi__select__search">
                                                                        <input type="text" placeholder="quick search" />
                                                                        <a href="#" class="under-navi__select__delete">
                                                                            <span>delete</span>
                                                                        </a>
                                                                    </div>
                                                                    <ul class="under-navi__select__list">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>                                                    
                                                    <dd class="cpu_fam2_container">
                                                        <div class="under-navi__select">
                                                            <input type="text" value="2 selected" data-dropdown="true" data-filter="cpu_fam2"/>
                                                            <div class="under-navi__select__dropdown cpu_fam2" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <div class="under-navi__select__search">
                                                                        <input type="text" placeholder="quick search" />
                                                                        <a href="#" class="under-navi__select__delete">
                                                                            <span>delete</span>
                                                                        </a>
                                                                    </div>
                                                                    <ul class="under-navi__select__list">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>-->
                                                    <dd class="cpu_excl_container" >
                                                        <input id="exclude" type="checkbox" radio-filter-name="cpu_family_excl" value="1" />
                                                        <label for="exclude" >Exclude selected Families</label>
                                                    </dd>
                                                </dl> 
                                            </li>
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">                                            
                                            <li class="under-navi__sub-list__item">
                                                <span>RAM Type:</span>
                                                <dl class="ram_type_dl">                                                                                                    
                                                </dl>   
                                            </li>
                                            <li class="under-navi__sub-list__item">
                                                <span>RAM Data Rate:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="under-navi__select">
                                                            <input type="text" value="3 selected" data-dropdown="true" data-filter="ram_rate"/>
                                                            <div class="under-navi__select__dropdown ram_rate" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <ul class="under-navi__select__list">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>
                                                </dl> 
                                            </li>
                                            <li class="under-navi__sub-list__item">
                                                <span>ECC RAM:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <input id="ram_ecc" type="checkbox" radio-filter-name="ram_ecc" value="1"/>
                                                         <label for="ram_ecc">ECC RAM</label>
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>No of installed HDDs</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <div class="global-slider">
                                                            <span data-slider="hdds-min"></span>
                                                            <div data-slider="true" data-slider-range="0,5" data-slider-value="2,4" data-slider-target="hdds-min,hdds-max" data-slider-name="hdd_num"></div>
                                                            <span data-slider="hdds-max"></span>
                                                        </div>  
                                                    </dd>
                                                </dl>
                                            </li> 
                                            <li class="under-navi__sub-list__item">
                                                <span>Max of HDDs</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="global-slider">
                                                            <span data-slider="hdds2-min"></span>
                                                            <div data-slider="true" data-slider-range="0,30" data-slider-value="0,30" data-slider-target="hdds2-min,hdds2-max" data-slider-name="hdd_max"></div>
                                                            <span data-slider="hdds2-max"></span>
                                                        </div> 
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>RAID:</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <input id="hardware" type="checkbox" radio-filter-name="raid_type" value="1"/>
                                                       <label for="hardware" >Hardware</label>
                                                    </dd>
                                                    <dd>
                                                       <input id="software" type="checkbox" radio-filter-name="raid_type" value="2"/>
                                                       <label for="software" >Software</label>
                                                    </dd>
                                                </dl>
                                            </li> 
                                            <li class="under-navi__sub-list__item">
                                                <span>RAID Type:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <input id="non-raid" type="checkbox" radio-filter-name="raid_mode" value="1"/>
                                                        <label for="non-raid" >Non-RAID</label>
                                                    </dd>
                                                    <dd>
                                                        <input id="raid0" type="checkbox" radio-filter-name="raid_mode" value="2"/>
                                                        <label for="raid0" >RAID0</label>
                                                    </dd>
                                                    <dd>
                                                        <input id="raid1" type="checkbox" radio-filter-name="raid_mode" value="3"/>
                                                        <label for="raid1" >RAID1</label>
                                                    </dd>
                                                    <dd>
                                                        <input id="raid5" type="checkbox" radio-filter-name="raid_mode" value="4"/>
                                                        <label for="raid5" >RAID5</label>
                                                    </dd>
                                                    <dd>
                                                        <input id="raid10" type="checkbox" radio-filter-name="raid_mode" value="5"/>
                                                        <label for="raid10" >RAID10</label>
                                                    </dd>
                                                </dl>
                                            </li>  
                                            <li class="under-navi__sub-list__item">
                                                <span>Battery Backup Unit:</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <input id="raid_bbu" type="checkbox" radio-filter-name="raid_bbu" value="1"/>
                                                       <label for="raid_bbu">Battery backup unit</label>
                                                    </dd>
                                                </dl>
                                            </li>           
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>No of IPv4 Address:</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <div class="global-slider">
                                                            <span data-slider="address-min"></span>
                                                            <div data-slider="true" data-slider-range="0,5" data-slider-value="2,4" data-slider-target="address-min,address-max" data-slider-name="address" data-slider-type=" IPv4"></div>
                                                            <span data-slider="address-max"></span>
                                                        </div>  
                                                    </dd>
                                                </dl>
                                            </li> 
                                            <li class="under-navi__sub-list__item">
                                                <span>No of IPv6 Address:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="global-slider">
                                                            <span data-slider="address2-min"></span>
                                                            <div data-slider="true" data-slider-range="0,5" data-slider-value="1,4" data-slider-target="address2-min,address2-max" data-slider-name="address2" data-slider-type=" IPv6"></div>
                                                            <span data-slider="address2-max"></span>
                                                        </div> 
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>Trafic:</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <input id="metered" type="checkbox" radio-filter-name="bw_type" value="1" />
                                                       <label for="metered" >Metered</label>
                                                    </dd>
                                                    <dd>
                                                       <input id="unmetered" type="checkbox" radio-filter-name="bw_type" value="2" />
                                                       <label for="unmetered" >Unmetered</label>
                                                    </dd>
                                                </dl>
                                            </li> 
                                            <li class="under-navi__sub-list__item">
                                                <span>Bandwidth:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="global-slider">
                                                            <span data-slider="bandwidth-min"></span>
                                                            <div data-slider="true" data-slider-range="0,15" data-slider-value="1,10" data-slider-target="bandwidth-min,bandwidth-max" data-slider-type="TB" data-slider-name="bw_num"></div>
                                                            <span data-slider="bandwidth-max"></span>
                                                        </div> 
                                                    </dd>
                                                </dl>
                                            </li>
                                            <li class="under-navi__sub-list__item">
                                                <span>Uplink:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="under-navi__select">
                                                            <input type="text" value="2 selected" data-dropdown="true" data-filter="bw_uplink"/>
                                                            <div class="under-navi__select__dropdown bw_uplink" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <ul class="under-navi__select__list">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner os_sub_list">
                                            <li class="os_sub_list_left"><ul>
                                                <li class="under-navi__sub-list__item">
                                                    <span>Operating System:</span>
                                                    <dl>                                                        
                                                        <dd>
                                                            <div class="under-navi__select">
                                                                <input type="text" value="2 selected" data-dropdown="true" data-filter="os"/>
                                                                <div class="under-navi__select__dropdown os" data-dropdown-target="true">
                                                                    <div class="under-navi__select__dropdown__inner">
                                                                        <ul class="under-navi__select__list">
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </dd>                                                    
                                                    </dl>
                                                </li>
                                            </ul></li>
                                            <li class="os_sub_list_right"><ul>
                                            </ul></li>
                                        </ul>                                       
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>Deployment Time:</span>
                                                <dl>                                                    
                                                    <dd>
                                                       <div class="global-slider">
                                                            <span data-slider="time-min"></span>
                                                            <div data-slider="true" data-slider-range="0,120" data-slider-value="0,120" data-slider-target="time-min,time-max" data-slider-type="min" data-slider-name="deployment"></div>
                                                            <span data-slider="time-max"></span>
                                                        </div>  
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                            <li class="under-navi__sub-list__item">
                                                <span>Hardware Add-Ons:</span>                                                    
                                                <dl class="add_ons_hw_dl">                                                    
                                                </dl>
                                            </li> 
                                            <li class="under-navi__sub-list__item">
                                                <span>Software Add-Ons:</span>     
                                                <dl class="add_ons_sw_dl">
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                           <li class="under-navi__sub-list__item">
                                                <span>Brands:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="under-navi__select">
                                                            <input type="text" value="2 selected" data-dropdown="true" data-filter="brands"/>
                                                            <div class="under-navi__select__dropdown brands" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <div class="under-navi__select__search">
                                                                        <input type="text" placeholder="quick search" />
                                                                        <a href="#" class="under-navi__select__delete">
                                                                            <span>delete</span>
                                                                        </a>
                                                                    </div>
                                                                    <ul class="under-navi__select__list">
                                                                       
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>
                                        <ul class="under-navi__sub-list__inner">
                                           <li class="under-navi__sub-list__item">
                                                <span>Allowed Content:</span>
                                                <dl>                                                    
                                                    <dd>
                                                        <div class="under-navi__select">
                                                            <input type="text" value="2 selected" data-dropdown="true" data-filter="content"/>
                                                            <div class="under-navi__select__dropdown content" data-dropdown-target="true">
                                                                <div class="under-navi__select__dropdown__inner">
                                                                    <div class="under-navi__select__search">
                                                                        <input type="text" placeholder="quick search" />
                                                                        <a href="#" class="under-navi__select__delete">
                                                                            <span>delete</span>
                                                                        </a>
                                                                    </div>
                                                                    <ul class="under-navi__select__list">
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </li>            
                                        </ul>                                                
                                    </div>
                                </div>    
                            </div>
                        </div>                   
                        <!-- header end -->
                    </div>
                </div>    
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <!-- page Start -->
                        <div class="ppage g-aligner __left">
                            <aside class="sidebar">
                                <div class="title-panel">
                                    <h2>Applied Filters</h2>
                                    <a href="#" class="clear-link">clear all</a>
                                </div>
                                <ul class="filter">
                                </ul>
                            </aside>
                            <section class="main-content">
                                <div class="main-content__inner">
                                    <ul class="main-content__panel">
                                        <li class="main-content__panel__item">
                                            <strong>0 Servers Found</strong>
                                        </li>
                                        <li class="main-content__panel__item">
                                            <nav class="main-content__panel__navi">
                                                <ul>
                                                    <li class="selected" id="">
                                                        <a href="#">View all</a>
                                                    </li>
                                                    <li id="0">
                                                        <a href="#">Deals</a>
                                                    </li>
                                                    <li id="1">
                                                        <a href="#">Clearance</a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </li>
                                        <li class="main-content__panel__item">
                                            <div class="table-search">
                                                <ul class="table-search__tags">                                                      
                                                </ul>
                                                <div class="table-search__text" contenteditable="true" >
                                                </div>
                                            </div>
                        
                                            <!--form class="search-form">
                                                <fieldset>
                                                    <input type="text" placeholder="enter server details" />
                                                </fieldset>
                                                <fieldset class="search-form__button">
                                                    <input type="submit" value="search" />
                                                </fieldset>    
                                            </form-->
                                        </li>
                                    </ul>
                                    <!-- CART -->
                                    <div class="cart-popup">
                                        <ul>
                                            <li><a href="#" class="add">ADD TO CART</a></li>
                                            <li><a href="#" class="checkout">CHECKOUT</a></li>
                                            <li><a href="#" class="configure">CONFIGURE</a></li>
                                        </ul>
                                        <a href="#" class="upper-filter__delecte">
                                            <span>delete</span>
                                        </a>
                                    </div>
                                    <!-- TABLE -->
                                    <table class="table __modified __hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <span class="sortable" id="cpu">CPU</span>
                                                </th>
                                                <th class="center">
                                                    <span class="sortable" id="cpu_score">
                                                        CPU<br />Score
                                                        <div class="question">
                                                            <div class="question__icon">?</div>
                                                            <div class="question__description">
                                                                <div class="question__description__inner">Specific Datacenter servers only datacenter servers only</div>
                                                                <div class="question__description__corm">&nbsp;</div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </th>
                                                <th class="center">
                                                    <span class="sortable" id="ram">Ram</span>
                                                </th>
                                                <th class="center">
                                                    <span class="sortable" id="hdd">HDD</span>
                                                </th>
                                                <th class="center">
                                                    <span class="sortable" id="traffic">Traffic</span>
                                                </th>
                                                <th class="center">
                                                    <span class="sortable" id="ip">IP</span>
                                                </th>
                                                <th class="center">Location</th>
                                                <th class="center">
                                                    <span class="sortable" id="price">Price</span>
                                                </th>
                                                <th class="center">
                                                    Action
                                                </th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="9" style="text-align:center"><img src="/wp-content/plugins/awesome-jquery-filters/assets/images/loader.gif"></td></tr>
                                        </tbody>
                                    </table>
                                    <ul class="page-navi">
                                        <li class="page-navi__item">
                                            <label for="show">Show</label>
                                            <select for="show">
                                                <option>20</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </li>
                                        <li class="page-navi__item">
                                            <ul id="paginator">
                                                <li class="page-navi__prev">
                                                    <a href="#">
                                                        <span>prev arrow</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">1</a>
                                                </li>
                                                <li>
                                                    <a href="#">2</a>
                                                </li>
                                                <li>
                                                    <a href="#">3</a>
                                                </li>
                                                <li>&hellip;</li>
                                                <li>
                                                    <a href="#">20</a>
                                                </li>
                                                <li>
                                                    <a href="#">21</a>
                                                </li>
                                                <li>
                                                    <a href="#">22</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li class="page-navi__next">
                                                    <a href="#">
                                                        <span>next arrow</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="page-navi__item">
                                            <label for="jump">Jump to</label>
                                            <input id="jump" type="text" />
                                        </li>
                                    </ul>
                                </div>    
                            </section>
                        </div>
                        <!-- page end -->  
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>