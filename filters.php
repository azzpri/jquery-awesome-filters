<?php
/*
Plugin Name: Awesome jQuery Filters
Plugin URI: http://hostboss.org
Description: Personal Plugin
Version: 1.0.0
Author: Sergey Vosrikov
Author URI: sergey.vostrikov@outlook.com
*/

define('AWESOME-JQUERY-FILTERS-PATH', dirname(__FILE__));

require( __DIR__ ."/products_new.php" );
require( __DIR__ ."/search_autocomplete.php" );

/**
 * Base function
 */
function filters() {
		
	wp_register_script( 'jquery2', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
    wp_enqueue_script( 'jquery2' );
	
	//wp_register_script( 'jquery-ui2', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
	//wp_enqueue_script( 'jquery-ui2' );
	
	//wp_register_script( 'jquery-ui2', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
	wp_register_script( 'jquery-ui2', plugin_dir_url( __FILE__ ) . 'includes/jquery-ui.min.js' );
	wp_enqueue_script( 'jquery-ui2' );
	
	wp_register_style( 'ajf-main-style', plugin_dir_url( __FILE__ ) . 'assets/css/main.css' );	
    wp_enqueue_style( 'ajf-main-style' );
	
	wp_register_script( 'awesome-jquery-filters', plugin_dir_url( __FILE__ ) . 'assets/js/awesome-jquery-filters-0.0.js' );
	wp_enqueue_script( 'awesome-jquery-filters' );
	
	wp_register_script( 'dedicated_servers', plugin_dir_url( __FILE__ ) . 'assets/js/dedicated_servers.js' );
	wp_enqueue_script( 'dedicated_servers' );
	
	wp_register_script( 'tag-caret-js', plugin_dir_url( __FILE__ ) . 'includes/tagEditor/jquery.caret.min.js' );
	wp_enqueue_script( 'tag-caret-js' );
	
	wp_register_script( 'tag-editor-js', plugin_dir_url( __FILE__ ) . 'includes/tagEditor/jquery.tag-editor.js' );
	wp_enqueue_script( 'tag-editor-js' );

	wp_register_style( 'tag-editor-css', plugin_dir_url( __FILE__ ) . 'includes/tagEditor/jquery.tag-editor.css' );	
    wp_enqueue_style( 'tag-editor-css' );
	
	wp_register_script( 'autocomplete-js', plugin_dir_url( __FILE__ ) . 'includes/jquery.autocomplete.min.js' );
	wp_enqueue_script( 'autocomplete-js' );


	echo '<script type="text/javascript">';
	echo 'var initial_data = '. json_encode( getServers() );
	echo '</script>';

    require( __DIR__ ."/html.php" );
}

// Register actions and shortcut
add_shortcode('awesome-jquery-filters', 'filters');
