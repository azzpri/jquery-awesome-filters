<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if( defined('DOING_AJAX') && DOING_AJAX ) {
	add_action('wp_ajax_nopriv_searchAutocomplete', 'searchAutocomplete_callback');
	add_action('wp_ajax_searchAutocomplete', 'searchAutocomplete_callback');
}

function searchAutocomplete_callback () {
	print_r( '{"sugesstions": ["Apple","Banana","Grape","Orange","Mango","Lemon","Pineapple","Watermelon"]}' );
	wp_die();
}