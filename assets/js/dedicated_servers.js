// Global currency init
if ( typeof CURRENCY === 'undefined' ){
	
	// check server currency and set it
	if ( typeof initial_data.currency !== 'undefined' ) {
		CURRENCY = initial_data.currency
		
	// if there is no server currency check	currency button in header
	} else {
		var btn = $('button.btn[data-id=wcnt-currency]');
		if ( btn.length > 0 && btn.attr('title').length > 0) 
			 CURRENCY = btn.attr('title');
		
		// set USD if there is no data in header	 
		else CURRENCY = 'USD';
	}
}

// set currency symbol
if ( typeof CURRENCY_SYMBOL === 'undefined' ) CURRENCY_SYMBOL = '$';

var ClientData = {
	csrf : "2321312eawewqeqe2qeq2e2qeq2eq2eq2e2qe2eq",
};
var FilterData = {};

function global_initial( ) {
	
	//SET CURRENCY_SYMBOL
	if ( typeof FilterData.currency[CURRENCY] !== 'undefined' )
		CURRENCY_SYMBOL = FilterData.currency[CURRENCY];
		
	//Get data from server
	GetServers = function ( callback ) {		
		if ( typeof(initial_data.items) !== 'undefined' && window.location.hash.length == 0 )
			if ( initial_data.items.length > 0 ) {
                var server_answer = initial_data;
				
				if ( typeof(server_answer.item_count) === 'undefined' )
                    server_answer.item_count = server_answer.items.length;
				
				if ( typeof(server_answer.page) === 'undefined' )
                    server_answer.page = 1;
					
				if ( typeof(server_answer.page_count) === 'undefined' )
                    server_answer.page_count = 1;	
								
				return callback( server_answer );
            }
					
		$.ajax( {
			type: "POST",
			url: "/wp-admin/admin-ajax.php?action=getServers" ,
			data: window.location.href,
			success: function ( response ) {				
				/* [
				 * 	"Dell",
				 * 	"hot_deal",
				 * 	"2 x Ittel Xeon E5620 8 cores x 3.3 GHz",
				 * 	"2344",
				 * 	"8GB ECC",
				 * 	"4x1TB-SATA2 2x500MB-SSD",
				 * 	"5000 GB 100MBs",
					"1",
					"Great Britan",
					"20"
					"/link to order/",
					"/link to cpu scores/"
					],
				*/
				var server_answer = jQuery.parseJSON(response);
				
				if ( typeof(server_answer.item_count) === 'undefined' )
                    server_answer.item_count = server_answer.items.length;
				
				if ( typeof(server_answer.page) === 'undefined' )
                    server_answer.page = 1;
					
				if ( typeof(server_answer.page_count) === 'undefined' )
                    server_answer.page_count = 1;	
								
				return callback( server_answer );
			}
		} );				

	}
    
	Views.f.cpu_procs = new TopFilter ( "cpu_procs" );
	
	Views.f.cpu_procs.group_name =  "CPU";
	
	//disable bunch auto sort
	Views.f.cpu_procs.sort_bunch =  false;
	
	
	Views.f.cpu_procs.data = new Core.Data( 
		"cpu_procs",
		function ( data ) {
			_this = Views.f.cpu_procs;
			
			var data = data.split(','),
				bunch_dict = {};
			
			$(_this.dom.procs).prop("checked", false);
			
			//check city checkboxes
			for ( var i in data ){
				var el = $(_this.dom.procs + "[value='"+ data[i] +"']");
				if( el.length != 0 ){
					el.prop("checked", true);
					bunch_dict[ data[i] ] = el.next().text();
				}
			}
			
			_this.changeInput();
			_this.bunchSet( bunch_dict );
			_this._reload_on_mouseleave = false;
		}		
	);
		
	Views.f.cpu_procs.dom = {
		input : "input[data-filter='cpu_procs']",
		cores : ".__cores > div > ul > li:last > ul input",
		procs : ".__cores > div > ul > li:first > ul input",
		checkboxes : ".__cores > div > ul > li > ul input",
		dropdown : ".__cores",
	};
		
	Views.f.cpu_procs.changeInput = function () {
		
		var
			procs = $( this.dom.procs + ":checked" ),
			cores = $( this.dom.cores + ":checked" );
		
		// 0
		if( cores.size() + procs.size() == 0 ){
			$(this.dom.input).val( this.null_input_value );
			return;
		}
		
		// 1 x 1
		if( cores.size() == 1 &&  procs.size() == 1 &&  procs.attr('value') != 'more' && cores.attr('value') != 'more' ) {
			$(this.dom.input).val( procs.val() + "x" + cores.val() + " Cores" );
			return;
		}
		
		$(this.dom.input).val( cores.size() + procs.size() + " Selected" );
	};
	
	Views.f.cpu_procs.clear = function () {
		$(this.dom.procs + ":checked").prop("checked", false);
		this.changeInput();
		this.bunchRemove();
		this.data.remove();
	};
	
	/** Initial function */
	Views.f.cpu_procs.init = function () {
	
		var _this = this;
		_this._reload_on_mouseleave = false;
		
		//change input value after change checkboxes
		$(_this.dom.checkboxes).change( function () {
			_this._reload_on_mouseleave = true;
			_this.changeInput();
		});
			
		$(_this.dom.dropdown).parent().parent().mouseleave( function () {
				
				$(_this.dom.dropdown).hide();
				
				//hide
				if( _this._reload_on_mouseleave ) {
					_this.afterChange();
				}
			}) 
			
		//close on mouseout & run update
		$(document)
			// on key up
			.keyup( function( event ){
		
				// check for parent div visible
				if( ! $( _this.dom.dropdown ).is(":visible") )
					return;
				
				// on esc
				if( event.which == 27 ){
					_this._reload_on_mouseleave = false;
					$( _this.dom.dropdown ).hide();
					_this.data.redraw();
				}
				
				//on enter
				if( event.which == 13 ){
					$( _this.dom.dropdown ).hide();
					_this.afterChange();
				}
			})
			.on( 'click', this.dom.input, function(){
					$('.upper-filter__dropdown-menu').hide();
					$( _this.dom.dropdown ).show();
			})
		;
			
		//clear on 'x'
		$(_this.dom.input).parent().parent().find('a').click( function () {
			$(_this.dom.checkboxes + ":checked").prop("checked", false);
			$(_this.dom.input).val( this.null_input_value );
			_this.data.remove( false );
			Views.f.cpu_cores.data.remove();
		});
	}
		
	/** Remove one value and make afterChange */
	Views.f.cpu_procs.removeValue = function ( checkbox_value ) {
		
		// unchecked input
		$( this.dom.procs + "[value=" + checkbox_value +"]" ).prop("checked", false);
		
		// run afterChange command to reload  
		this.afterChange();
	}
	
	/** Actions after change come value */
	Views.f.cpu_procs.afterChange = function (  ) {
		
		var 
			cores_bunch_dict = {},
			cores_param_string = "",
			procs_bunch_dict = {},
			procs_param_string = ""
			_this = this;
		
		//create URL string
		function _addValue ( el, dict, string ) {
			if( string.length > 0 )
				string += ',';
			string += el.val();
			dict[ el.val() ] = el.next().text();
			
			return string;
		}
		
		$(_this.dom.checkboxes + ":checked").each( function () {
		
			if( $(this).attr('name') == _this.name ) {
				procs_param_string = _addValue( $(this), procs_bunch_dict, procs_param_string  );
			} else {
				cores_param_string = _addValue( $(this), cores_bunch_dict, cores_param_string  );
			}
		});
	
		// add values to bunch
		this.bunchSet( procs_bunch_dict );
		Views.f.cpu_cores.bunchSet( cores_bunch_dict );
			
		// add values to data
		Views.f.cpu_cores.data.set( cores_param_string, false );
		this.data.set( procs_param_string );
	};
	
	Views.f.cpu_procs.init();
	
	/**
		HDD Type
	*/
	Views.f.hdd_type = new TopFilter ( "hdd_type" );
	
	Views.f.hdd_type.group_name =  "HDD";
	
	//disable bunch auto sort
	Views.f.hdd_type.sort_bunch =  false;
	
	Views.f.hdd_type.data = new Core.Data( 
			"hdd_type",
			function ( data ) {
				_this = Views.f.hdd_type;
				
				var data = data.split(','),
					bunch_dict = {};
				
				$(_this.dom.checkboxes).prop("checked", false);
				
				//check city checkboxes
				for ( var i in data ){
					var el = $(_this.dom.checkboxes + "[value='"+ data[i] +"']");
					if( el.length != 0 ){
						el.prop("checked", true);
						bunch_dict[ data[i] ] = el.next().text();
					}
				}
				
				_this.changeInput();
				_this.bunchSet( bunch_dict );				
				_this._reload_on_mouseleave = false;
			}
		);
		
	Views.f.hdd_type.dom = {
			dropdown : ".__type",
			input : "input[data-filter='hdd_type']",
			checkboxes : ".__type input",
		};
		
	Views.f.hdd_type.changeInput = function () {
		var checked = $( this.dom.checkboxes + ":checked");
			
		if( checked.size() == 1  ){
			$(this.dom.input).val( checked.next().text().toUpperCase() );
		} else {
			$(this.dom.input).val( checked.size() + " Selected" );
			
			this.setNullValueToInput( checked.size(), this.dom.input, true );
		}
		
	};
	
	Views.f.hdd_type.clear = function () {
		$(this.dom.checkboxes + ":checked").prop("checked", false);
		this.changeInput();
		this.bunchRemove();
		this.data.remove();
	};
		
	Views.f.hdd_type.init = function () {
			
		var _this = this;
		_this._reload_on_mouseleave = false;
		
		//change input value after change checkboxes
		$(_this.dom.checkboxes).change( function () {
			_this._reload_on_mouseleave = true;
			_this.changeInput();
		});
			
		
		//close on mouseout & run update
		$(_this.dom.dropdown).parent().parent().mouseleave( function () {
		
		
			//hide
			$(_this.dom.dropdown).hide();
				
			if( _this._reload_on_mouseleave ){	
				_this.afterChange();
			}
		}) 
		
		$(document)
			// on key up
			.keyup( function( event ){
			
				// check for parent div visible
				if( ! $( _this.dom.dropdown ).is(":visible") )
					return;
				
				// on esc
				if( event.which == 27 ){
					$( _this.dom.dropdown ).hide();
					_this._reload_on_mouseleave = false;
					_this.data.redraw();
				}
				
				//on enter
				if( event.which == 13 ){
					$( _this.dom.dropdown ).hide();
					_this.afterChange();
				}
			})
			.on( 'click', this.dom.input, function(){
				$('.upper-filter__dropdown-menu').hide();
				$( _this.dom.dropdown ).show();
			})
		;
		
		//clear on 'x'
		$(_this.dom.input).parent().parent().find('a').click( function () {
			$(_this.dom.checkboxes + ":checked").prop("checked", false);
			$(_this.dom.input).val( "-" );
			_this.data.remove();
		});
	};
		
	/** Remove one value and make afterChange */
	Views.f.hdd_type.removeValue = function ( checkbox_value ) {
		
		// unchecked input
		$( this.dom.checkboxes + "[value=" + checkbox_value +"]" ).prop("checked", false);
		
		// run afterChange command to reload  
		this.afterChange();
	}
	
	/** Actions after change some value */
	Views.f.hdd_type.afterChange = function ( ) {
		
		var bunch_dict = {};
		var _this = Views.f.hdd_type;
				
		//create URL string
			var string = "";
			
			//checkboxes					
			$(_this.dom.checkboxes + ":checked").each( function () {
				if( string.length > 0 )
					string += ',';
											
				string += $(this).val();
				bunch_dict[ $(this).val() ] = $(this).next().text();
			});
			
			_this.data.set( string );
				
		
		//reload
		this.bunchSet( bunch_dict );
		this.data.set( string );
				
	};
	
	Views.f.hdd_type.init();
	
	/**
		HDD SIZE
	*/
	$("div[data-slider-name='hdd_size']").attr(
		'data-slider-range',
		FilterData.hdd_size.steps[0] +','+ FilterData.hdd_size.steps[ FilterData.hdd_size.steps.length - 1] );
	
	Views.f.Storage = new Slider( {
		name : "hdd_size", 
		group_name : "HDD", 
		input_selector : "input[data-filter='hdd_size']",
		pop_up_container_selector : ".hdd_size_container",
		slider_options : FilterData.hdd_size
	} );
	
	/**
		HDD NUM
	*/
	Views.f.hdd_num = new Slider ({
		name : "hdd_num", 
		group_name : "HDD",
		slider_options : FilterData['hdd_num'],
		dimensions : [
				new Dimension( {
					value : ' inst.',
					display_in : {
						slider : false,
						applied_in_input : true, 
						applied_after_input : false,
						input : false,
					}
				} ),
			]	
	});
	/**
		HDD MAX
	*/
	Views.f.hdd_max = new Slider ({
		name : "hdd_max", 
		group_name : "HDD",
		slider_options : FilterData['hdd_max'],
		dimensions : [
			new Dimension( {
				value : ' max.',
				display_in : {
					slider : false,
					applied_in_input : true, 
					applied_after_input : false,
					input : false,
				}
			} ),
		]	
	});
	
	$("div[data-slider-name='deployment']").attr(
		'data-slider-range',
		FilterData.deployment.steps[0] +','+ FilterData.deployment.steps[ FilterData.deployment.steps.length - 1] );
	
	Views.f.Storage = new Slider( {
		name : "hdd_size", 
		group_name : "HDD", 
		input_selector : "input[data-filter='hdd_size']",
		pop_up_container_selector : ".hdd_size_container",
		slider_options : FilterData.hdd_size
	} );
	
	Views.f.deployment = new Slider( {
		name : "deployment", 
		group_name : "Deployment",
		slider_options : FilterData['deployment'],
		input_selector : "input[data-filter='deployment']",		
		//dimensions : [
		//	new Dimension( {
		//		value : " H",
		//		display_in : {
		//			slider : true,
		//			applied_in_input : true, 
		//			applied_after_input : false,
		//		}
		//	} ),
		//]
	} );
	
	Views.f.bw_type = new Checkbox( {
		name : "bw_type", 
		group_name : "Bandwidth",
	} );
	
	Views.f.bw_num = new Slider( {
		name : "bw_num", 
		group_name : "Bandwidth",
		slider_options : FilterData['bw_vol'],
		dimensions : [
			new Dimension( {
				value : "TB",
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
				}
			} ),
		]
	} );
	
	Views.f.bw_vol = new SearchList ({
		name : "bw_uplink",
		group_name : "Bandwidth",
		search_options : {
			display_elements : 5,
			data : FilterData.bw_uplink
		}	
	});
	
	Views.f.ram_size = new Slider( {
		name : "ram_size", 
		group_name : "RAM",
		input_selector : "input[data-filter='ram_size']",
		pop_up_container_selector : ".ram_size_container",
		slider_options : FilterData['ram_size'],
		dimensions : [
			new Dimension( {
				value : "GB",
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
					input : true,
				}
			} ),
		]
	} );
		
	Views.f.ram_rate = new SearchList ({
		name : "ram_rate",
		group_name : "RAM",
		search_options : {
			display_elements : 7,
			data : FilterData.ram_rate
		}	
	});
	
	
	Views.f.ram_ecc = new Checkbox( {
	 name : "ram_ecc", 
	 group_name : "RAM"
	} );
	
	Views.f.address = new Slider( {
		name : "address", 
		group_name : "IP Addresses",
		slider_options : FilterData['address'],
		dimensions : [
			new Dimension( {
				value : " IPv4",
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
				}
			} ),
		]
	} );
	
	Views.f.address2 = new Slider( {
		name : "address2", 
		group_name : "IP Addresses",
		slider_options : FilterData['address2'],
		dimensions : [
			new Dimension( {
				value : " IPv6",
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
				}
			} ),
		]
	} );
	
	Views.f.Cost = new Slider( {
		name : "server_cost", 
		group_name : "Cost",
		input_selector : "input[data-filter='server_cost']",
		pop_up_container_selector : ".server_cost_container",
		slider_options : FilterData['cost'],
		
		dimensions : [
			new Dimension( {
				//value : FilterData.cost.value,
				value : CURRENCY_SYMBOL,
				position : FilterData.cost.position,
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
					input : true,
				}
			} ),
			new Dimension( {
				value : "/mo",
				display_in : {
					slider : true,
					applied_in_input : true, 
					applied_after_input : false,
					input : false,
				}
			} ),
		]
	
	} );
	
	 
	 Views.f.country = new TopFilter ( "countries" );
	
	Views.f.country.group_name =  "Country";
	
	Views.f.country.data = new Core.Data( 
			"countries",
			function ( data ) {
					
				_this = Views.f.country;
				
				var data = data.split(',');
				var bunch_dict = {};
				
				$(_this.dom.city).prop("checked", false);
	
				//check city checkboxes
				for ( var i in data ){
					var el = $(_this.dom.city + "[value='"+ data[i] +"']");
					if( el.length != 0 ){
						el.prop("checked", true);
						bunch_dict[ data[i] ] = el.next().text();	
					}
				}
				
				//check if in current country 0 city selected
				_this.changeCountryInput();
		
				_this.bunchSet( bunch_dict );
				_this.changeInput();
			}		
		);
		
	Views.f.country.dom = {
			dropdown : ".__city",
			input : "input[data-filter='countries']",
			region : ".__city > div > ul > li",
			country : ".__city > div > ul > li > div > input",
			city : ".__city > div > ul > li > ul input",
		};
	
	Views.f.country.changeInput = function () {
		// if 0 country selected
		this.setNullValueToInput( $(this.dom.city + ":checked").size(), this.dom.input );
		
		// if 1 country selected display this name
		if( $(this.dom.city + ":checked").size() == 1 )
			$( this.dom.input ).val( $(this.dom.city + ":checked").next().text() );
		};
	
	Views.f.country.clear = function () {
		$(this.dom.city + ":checked").prop("checked", false);
		$(this.dom.country + ":checked").prop("checked", false);
		this.changeInput();
		this.bunchRemove();
		this.data.remove();
	};
	
	/** Check for city count, change country checkbox */
	Views.f.country.changeCountryInput = function (){
	
		// clear country checkbox
		$(this.dom.country + ":checked").prop("checked", false);
		$(this.dom.country).prop("indeterminate", false);
		
		$(this.dom.country).each( function(){ 
			var checked_city_count = $(this).parent().next().find("input:checked").size();
			var city_count = $(this).parent().next().find("input").size();
			
			// full check
			if( city_count == checked_city_count ){
				$(this).prop("checked", true);
			}else{
				// set indeterminate
				if( checked_city_count > 0 )
					$(this).prop("indeterminate", true);
			}
			
		});
	};
	
	Views.f.country.init = function () {
			
			var _this = this;
			
			// change sub checkboxes after change group checkbox
			$(_this.dom.country).change( function () {
				if ( $(this).is(":checked") ){
					$(this).parent().parent().find("ul input").prop( "checked", true );
				} else {
					$(this).parent().parent().find("ul input").prop( "checked", false );
				}
				
				//change input value after change input
				_this.changeInput();
			});
			
			//change input value after change city input
			$(_this.dom.city).change( function () {
				_this.changeInput();
				_this.changeCountryInput();
			});
			
			// on key up
			$(document)
				.keyup( function( event ){
			
					// check for parent div visible
					if( ! $( _this.dom.dropdown ).is(":visible") )
						return;
					
					// on esc
					if( event.which == 27 ){
						$( _this.dom.dropdown ).hide();
						_this._reload_on_mouseleave = false;
						_this.data.redraw();
					}
					
					//on enter
					if( event.which == 13 ){
						$( _this.dom.dropdown ).hide();
						_this.afterChange();
					}
				}) 
				.on('click', this.dom.input, function(){
					$('.upper-filter__dropdown-menu').hide();
					$( _this.dom.dropdown ).show();
					_this._reload_on_mouseleave = true;
				})
			;	
			//close on mouseout & run update
			$(_this.dom.dropdown).parent().parent().mouseleave( function () {
				//hide div with checkboxes
				if( _this._reload_on_mouseleave ){
					$(_this.dom.dropdown).hide();
					_this.afterChange();
				}
			});
			
			//clear on 'x'
			$(_this.dom.input).parent().parent().find('a').click( function () {
				_this.clear();
			});
		};
		
	/** Remove one value and make afterChange */
	Views.f.country.removeValue = function ( checkbox_value ) {
		
		// unchecked input
		$( this.dom.city + "[value=" + checkbox_value +"]" ).prop("checked", false);
		
		// run afterChange command to reload  
		this.afterChange();
	}
	
	
	/** Actions after change come value */
	Views.f.country.afterChange = function ( ) {
		
		//create URL string & bunch array
		var string = "";
		var bunch_dict = {};
		$(this.dom.city + ":checked").each( function () {
			if( string.length > 0 )
				string += ',';
			string += $(this).val();
			bunch_dict[ $(this).val().toString() ] = $(this).next().text();
		});
		
		//reload
		this.bunchSet( bunch_dict );
		this.data.set( string );
				
	};
	
	Views.f.country.init();
	
	Views.f.os = new SearchList ({
		name : "os",
		group_name : "OS",
		search_options : {
			display_elements : 4,
			data : FilterData.os,
		},
	
		clear1 : function () {
			$( Views.f.os1.depend_on.container_selector ).hide();
			$( Views.f.os2.depend_on.container_selector ).hide();
		}	
	});
				
	Views.f.raid_type = new Checkbox( {
	 name : "raid_type", 
	 group_name : "RAID",
	} );
	
	Views.f.raid_mode = new Checkbox( {
	 name : "raid_mode", 
	 group_name : "RAID",
	 multiple_select : true,
	} );
	
	Views.f.raid_bbu = new Checkbox( {
	 name : "raid_bbu", 
	 group_name : "RAID"
	} );
	
	Views.f.content = new SearchList ({
		name : "content",
		group_name : "Allowed Content",
		search_options : {
			display_elements : 4,
			data : FilterData.content,
		}	
	});
	
	Views.f.brands = new SearchList ({
		name : "brands",
		group_name : "Brands",
		search_options : {
			display_elements : 4,
			data : FilterData.brands,
		}	
	});
	
	Views.f.cpu_cores = new TopFilter ( "cpu_cores" );
	
	Views.f.cpu_cores.group_name =  "CPU";
	
	//disable bunch auto sort
	Views.f.cpu_cores.sort_bunch =  false;
	
	
	Views.f.cpu_cores.data = new Core.Data( 
		"cpu_cores",
		function ( data ) {
			_this = Views.f.cpu_cores;
			
			var data = data.split(','),
				bunch_dict = {};
			
			$(_this.dom.cores).prop("checked", false);
			
			//check city checkboxes
			for ( var i in data ){
				var el = $(_this.dom.cores + "[value='"+ data[i] +"']");
				if( el.length != 0 ){
					el.prop("checked", true);
					bunch_dict[ data[i] ] = el.next().text();
				}
			}
			
			_this.changeInput();
			_this.bunchSet( bunch_dict );
		}		
	);
		
	Views.f.cpu_cores.dom = {
		input : "input[data-filter='cpu_procs']",
		cores : ".__cores > div > ul > li:last > ul input",
	};
		
	Views.f.cpu_cores.changeInput = function() {
		Views.f.cpu_procs.changeInput();
	};
	
	Views.f.cpu_cores.clear = function () {
		$(this.dom.cores + ":checked").prop("checked", false);
		this.changeInput();
		this.bunchRemove();
		this.data.remove();
	};
		
	Views.f.cpu_cores.init = function () {}
		
	/** Remove one value and make afterChange */
	Views.f.cpu_cores.removeValue = function ( checkbox_value ) {
		
		// unchecked input
		$( this.dom.cores + "[value=" + checkbox_value +"]" ).prop("checked", false);
		
		// run afterChange command to reload  
		this.afterChange();
	}
	
	/** Actions after change come value */
	Views.f.cpu_cores.afterChange = function (  ) {
		Views.f.cpu_procs.afterChange();
	};
	
	Views.f.cpu_cores.init();
	
	addExtraFilters();

	/* My */
	Core.init();
	Views.init();
	Core.update();
	$(document).on( "click", 'a[href=#]',  function( e ){
		 e.preventDefault();
	});
	
}

function addOSFilter ( os_name, i ) {	
	var li = $('<li class="under-navi__sub-list__item os'+i+'_container"><span>'+os_name+':</span><dl><dd></dd></dl></li>').appendTo(".os_sub_list_right > ul");
	var div_sel = $('<div class="under-navi__select"><input type="text" value="0 selected" data-dropdown="true" data-filter="os' + i +'"/></div>').appendTo( li.find( 'dd' ));                                                            
	var div_dd = $('<div class="under-navi__select__dropdown os' + i +'" data-dropdown-target="true"></div>').appendTo( div_sel );
	$('<div class="under-navi__select__dropdown__inner"><ul class="under-navi__select__list"></ul></div>').appendTo( div_dd );	
}

function addExtraFilters() {
	
	// RAM Type
	if ( typeof(FilterData.ram_type) !== 'undefined' ) {
		for( var i in FilterData.ram_type ){
			var ram_name = FilterData.ram_type[i];
			$('<dd><input id="ddr'+i+'" type="checkbox" value="'+i+'" radio-filter-name="ram_type"/><label for="ddr'+i+'" >'+ram_name+'</label></dd>').appendTo(".ram_type_dl");	
		}
	}	
	
	Views.f.ram_type = new Checkbox( {
		name : "ram_type", 
		group_name : "RAM",
		multiple_select : true,
	} );
	
	//HW addons
	if ( typeof(FilterData.add_ons_hw) !== 'undefined' ) {
		for( var i in FilterData.add_ons_hw ){
			var addon_name = FilterData.add_ons_hw[i];
			$('<dd><input id="add_ons_hw'+i+'" type="checkbox" value="'+i+'" radio-filter-name="add_ons_hw"/><label for="add_ons_hw'+i+'" >'+addon_name+'</label></dd>').appendTo(".add_ons_hw_dl");	
		}
	}
	Views.f.add_ons_hw = new Checkbox( {
	 name : "add_ons_hw", 
	 group_name : "Add-Ons",
	 multiple_select : true,
	} );
	
	//SW addons
	if ( typeof(FilterData.add_ons_sw) !== 'undefined' ) {
		for( var i in FilterData.add_ons_sw ){
			var addon_name = FilterData.add_ons_sw[i];
			$('<dd><input id="add_ons_sw'+i+'" type="checkbox" value="'+i+'" radio-filter-name="add_ons_sw"/><label for="add_ons_sw'+i+'" >'+addon_name+'</label></dd>').appendTo(".add_ons_sw_dl");	
		}
	}
	
	Views.f.add_ons_sw = new Checkbox( {
	 name : "add_ons_sw", 
	 group_name : "Add-Ons",
	 multiple_select : true,
	 } );
	
	// OS
    if ( typeof(FilterData.os) !== 'undefined' ) {
								
		for ( var i in FilterData.os ){
			
			var os_name = FilterData.os[i];
			
			if ( typeof(FilterData['os'+i]) !== 'undefined' ) {				
				
				// add html structure
				addOSFilter( os_name, i );
												
				// add data
				Views.f['os'+i] = new SearchList ({
					name : 'os'+i,
					group_name : "OS",
					null_input_value : "any",
					depend_on : {
						filter : Views.f.os,
						data_id: i,
						container_selector : ".os"+i+"_container",
					},
					search_options : {
						display_elements : FilterData['os'+i].length,
						data : FilterData['os' + i]
					},	
					
					// rewrite default extra after change input function
					afterChangeInput : function (){
						var appl_el = Views.FilterBunch.groups.OS.filter_list
								.find("li[filter-name='" + Views.f.os.name + "'][filter-id='"+ Views.f['os'+i].depend_on.data_id +"']");
								
						if (  appl_el.length > 0 )		
							if (  $( this.dom.checkboxes + ":checked" ).size() > 0 )
								appl_el.hide();
							else
								appl_el.show();
					},
				});
			}
		}
	}
	

	// CPU Manufacture & Family	
    if ( typeof( FilterData.cpu_brand ) !== 'undefined' ) {
				
		var dl = $(".cpu_brand_li dl");
		var fam_li  = $(".cpu_fam_li");
		
		// create inputs in table	
		for ( var i in FilterData.cpu_brand ) {			
			var value = FilterData.cpu_brand[i];
			
			var dd = $('<dd>').appendTo(dl);
			
			$('<input type="checkbox">')
				.attr({
					id 					: value,
					'radio-filter-name' : 'cpu_brand',
					value 				: i })
				.appendTo(dd);
			$('<label>')
				.attr({ for : value })
				.text( value )
				.appendTo(dd);
		}
		
		Views.f.manufacture = new Checkbox( {
			name : "cpu_brand", 
			group_name : "CPU",
			
		} );
		
		//create cpu_fam elements
		for ( var i in FilterData.cpu_brand ) {		
			
			$("<span>").addClass('cpu_fam'+ i +'_container').text(' CPU Family:').prependTo(fam_li);
			
			var div_sel = $('<dd class=cpu_fam'+i+'_container><div class="under-navi__select"><input type="text" value="0 selected" data-dropdown="true" data-filter="cpu_fam' + i +'"/></div>').prependTo( fam_li.find('dl') );
			var div_dd = $('<div class="under-navi__select__dropdown cpu_fam' + i +'" data-dropdown-target="true"></div>').appendTo( div_sel.find('.under-navi__select') );
			$('<div class="under-navi__select__dropdown__inner"><ul class="under-navi__select__list"></ul></div>').appendTo( div_dd );	
	
			
			Views.f['cpu_fam' + i] = new SearchList ({
				name : "cpu_fam" + i,
				group_name : "CPU",
				depend_on : {
					filter : Views.f.manufacture,
					container_selector : ".cpu_fam"+ i +"_container",
					data_id: i,
				},
				search_options : {
					display_elements : 5,
					data : FilterData['cpu_fam'+i],
				},
			
				// rewrite default extra after change input function
				afterChangeInput : function (){
					var appl_el = Views.FilterBunch.groups.CPU.filter_list
							.find("li[filter-name='" + Views.f.manufacture.name + "'][filter-id='"+ Views.f['cpu_fam'+i].depend_on.data_id +"']");
							
					if(  appl_el.length > 0 )		
						if(  $( this.dom.checkboxes + ":checked" ).size() > 0 )
							appl_el.hide();
						else
							appl_el.show();
				},	
			});
		}
			
		Views.f.cpu_family_excl = new Checkbox( {
			name : "cpu_family_excl", 
			group_name : "CPU",
			depend_on : {
				filter : Views.f.manufacture,
				container_selector : ".cpu_excl_container",
			},
		} );
	}
	
	Views.f.table_search = new Checkbox( {
		name : "table_search", 
		group_name : "Search",
		multiple_select : true,
		bool_clear_all : false,
		data : new Core.Data( "table_search", function (data) {
		
			_this = Views.Table.Search;
			
			if ( ! _this._after_init ) 
				return;
					
			_this._after_init = false;
			
			var dt = data.split(",");
			
			for( var i in dt ){
				if( dt[i].length > 0 ) 
					$( _this.dom.select ).tagEditor('addTag', decodeURIComponent(dt[i]));	
			}								
		}),
		removeValue : function ( checkbox_value ) {
			
			var tag_value = $( Views.FilterBunch.dom.sidebar )
				.find( 'li[filter-name=table_search][filter-id='+ checkbox_value +']' ).text();
			
			$( Views.Table.Search.dom.select ).tagEditor('removeTag', tag_value );	
			// run depend functions
			for( var i in this.dependCheckFnList ){
				this.dependCheckFnList[i][1].clear( false ); 
				$(this.dependCheckFnList[i][1].depend_on.container_selector).hide();
			}
			
			// run afterChange command to reload  
			this.afterChangeInput();
			this.afterChange();
		},
		afterChange : function (){
			var tags = $( Views.Table.Search.dom.select ).tagEditor('getTags')[0].tags;
			var string = (tags.length ? tags.join(',') : '');
			Views.f.table_search.bunchSet( tags ); 
			Views.f.table_search.data.set( string );								
		},
		clear : function ( reload ) {
			
			this.bool_clear_all = true;
			
			var tags = $( Views.Table.Search.dom.select ).tagEditor('getTags')[0].tags;
			for (i = 0; i < tags.length; i++) {
				$(Views.Table.Search.dom.select).tagEditor('removeTag', tags[i]);
			}									
			this.bool_clear_all = false;
			
			this.bunchRemove();
			this.data.remove( reload );
		},		
		
	});
}
					
$(document).ready(function(){	
	$.getJSON( "/wp-content/plugins/awesome-jquery-filters/assets/js/filters_map.json", function( data ) {		
		FilterData = data;
			
		// initialization
		global_initial();
	  });
			
});