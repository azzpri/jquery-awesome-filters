var GetServers = function(){};

var dbg = function ( text ) {
	console.log( text );
};

/** Global functions and API's
*/
var Core = {
	// Global storage
	storage : {
		"page" : 1,
		"page_count" : 1,
	},

	initial_URL : -1,
	hash_map : {},
	
	/** Get data from server and redraw table and filters
	*/
	update : function(){
		
		// check current URL with initial. 
		if( this.initial_URL == window.location.hash )
			return;
		
		// sync data with server
		var data = Core.sync( function(data) {
								
			// Update data in table
			Views.Table.draw( data.items );
			
			// display item count
			Views.Table.displayItemCount( data.item_count );
			
			Core.storage.page = data.page;
			Core.storage.page_count = data.page_count;
			
			Core.hash_map = Core.URLparser.parseHash();
			
			// Update filters view
			for ( var i in Core.DataSet.data_obj_list ){
				
				var data_obj = Core.DataSet.data_obj_list[i];
				var name = data_obj.name;
				
				var have_value = false;
				
				for ( var j in Core.hash_map ){
					if( j == name ){
						data_obj.draw( Core.hash_map[j][0] );
						have_value = true;
						break;
					}
				}
				
				if ( !have_value )
					data_obj.draw( "" );
	
			}
			
			// store initial URL
			this.initial_URL = window.location.hash;	
		});
		
	},
};

/** Sync & Data processing
*/
Core.Sync = {
	init : function (){		
		Core.sync = this._sync
	},
 	getDataFromServer : function () {
	},
	_sync : function( callback ) {		
		return GetServers( callback )
	},
	
	/** Send user filters data to server */
	sendUserFilters : function () {
		var filter_data = '';		
		localStorage.setItem('AWS-myFilters', JSON.stringify(Core.UserFiltersData.filter_data));
	},
}


/** Storade of Data objects and filters param
*/
Core.DataSet = {
	//storage : {},
	data_obj_list : []
}
/** Filter data abstract class. Add data to storage, work with URL #.
*/
Core.Data = function ( name, draw_func ) {

	this.name = name;
	
	if ( typeof( draw_func ) === 'undefined' ){
		this.draw = function ( a ){ return; } ;
	} else {
		this.draw = draw_func;
	}
	
	Core.DataSet.data_obj_list.push( this );	
	
	/** store data
	*/
	this.set = function ( value, update ) {
		if ( value.length == 0 ) {
			this.remove( this.name );
			Core.URLparser.remove( this.name );
		} else {
			//Core.DataSet.storage[ name ] = value;
		
			//add attr to URL # tag
			Core.URLparser.set( this.name, value );
		}
		
		if( typeof( update ) === 'undefined' )
			var update = true;
			
		if( update )	
			Core.update();
	};

	/** redraw current data */
	this.redraw = function ( ) {
			
		for( v in Core.hash_map ){
			if ( v == name )
				this.draw( Core.hash_map[v][0] );
		}
	};
	
	this.remove = function ( update ) {
	
		//delete Core.DataSet.storage[ this.name ];		
		//remove attr from URL # tag
		Core.URLparser.remove( this.name );
				
		if( typeof( update ) === 'undefined' )
			update = true;
			
		if( update )	
			Core.update();
	}
	
}

/** Gears to work with URL parsing and # params
*/
Core.URLparser = {

	/** Set param to URL string
		@name - string. Param name.
		@value - string. Param value (ex. "1,2,3").
	*/
	set : function ( name, value ) {

		var current_value = this.search( name );
		
		this.remove ( name, current_value );
		this.add( name, value );
	},
	
	/** Just remove param from URL string
		@name - string. Param name.
		@value - string. Param value (ex. "1,2,3"), not required.
	*/	
	remove : function ( name, value ) {
	
		//find current value if value is undefined
		if( typeof( value ) === 'undefined' )
			var value = this.search( name );

		// remove current value from URL string
		
		var hash = window.location.hash;
		
		hash = hash.replace( "&" + name + "=" + value, "" );
		hash = hash.replace( name + "=" + value, "" );
		hash = hash.replace( "#&", "#" );
							    	
		var scrollV, scrollH, loc = window.location;
		// Prevent scrolling by storing the page's current scroll offset
		scrollV = document.body.scrollTop;
		scrollH = document.body.scrollLeft;

		window.location.hash = hash

		// Restore the scroll offset, should be flicker free
		document.body.scrollTop = scrollV;
		document.body.scrollLeft = scrollH;
		
				
	},
	
	/** return params current value from URL string  
	*/
	search : function ( name ) {
	
		var hash_map = this.parseHash();
		
		for( v in hash_map ){
			if ( v == name )
				return hash_map[v];
		}
		
		return -1;
	},

	/** Just add param to URL string
		@name - string. Param name.
		@value - string. Param value (ex. "1,2,3").
	*/	
	add : function ( name, value ) {
	
		if ( window.location.hash.length != 0 )
			window.location.hash += '&';
			
		window.location.hash += name + "=" + value;
	},
	
	/** Get hash string from URL, parse to map
	*/
	parseHash : function ( query ) {
		
		if( typeof( query ) === 'undefined' )
			var query = window.location.hash.substr(1);
			
		var map = {};
		
		query.replace(/([^&=]+)=?([^&]*)(?:&+|$)/g, function(match, key, value) {
			(map[key] = map[key] || []).push(value);
		});
		return map;
	},
}

Core.init = function () {	
	Core.Sync.init();
}

Core.Class = new function ( attrs ) {
	
	this.name = "Class";
	this.required_attrs = ['name'];
	
	this.init = function( attrs ){ 	
		
		// set attributes from attrs
		for( var i in attrs )
			if( typeof( attrs[i] )  !== 'undefined' ) {
				this[i] = attrs[i];
			}
		
		// check for required attrs
		for( var i = 0; i < this.required_attrs.length; i++ ) {
			if( typeof( attrs[ this.required_attrs[i] ] )  === 'undefined' ) {
				alert("Attribute " + this.required_attrs[i] + " not found in class '" + this.name + "'!");
			}
		
		}
	};
	
};

var Views = {
}

/** Filters front part
*/
Views.Filters = {
	init : function () {
		this.Top.init();
	},
}

/** Top filters panel
*/
Views.Filters.Top = {

	init : function() {
		this.MoreFilters.init();
		this.Navigation.init();
	}
}

/** Top filtes "More Filters" button
*/
Views.Filters.Top.MoreFilters = {
		
	dom : {
		button : ".upper-filter__button",
		navi : ".under-navi",
		sub : ".under-navi__sub-list",
	},
	
	init : function () {
	
		var _this = this;
		
		// on click button
		//$( _this.dom.button ).click( function () {
		//	
		//	// change styles & hide sub bar
		//	if ( ! $(_this.dom.navi).is(":visible") ){
		//
		//		$(this).text("Less filters");
		//		$(this).css({"color" : "black", "background" : "white", "border" : "1px solid #939598"});
		//		
		//	} else {
		//		$(this).text("More filters");
		//		$(this).css({"color" : "white", "background" : "black", "border": "none"});
		//		
		//		// reinitialize navigation 
		//		$(Views.Filters.Top.Navigation.dom.navi_li).removeClass("seleced");
		//		$(Views.Filters.Top.Navigation.dom.sub).css("display", "none");
		//	}
		//	
		//	// toggle visiblity
		//	$(_this.dom.navi).toggle();
		//})
	},
}

/** Top filters navigation panel 
*/
Views.Filters.Top.Navigation = {

	dom : {
		navi : ".under-navi",
		navi_li : ".under-navi__item",
		navi_li_a : ".under-navi__item a",
		navi_li_a_before : ".under-navi__item a:before",
		sub : ".under-navi__sub-list",
		sub_li : ".under-navi__sub-list__inner",
		
	},
	
	/** Initial function
	*/
	init : function () {
		
		var _this = this;
		
		// On click filter label
		$(this.dom.navi_li).click( function () {
		
			if ( $(this).hasClass('seleced') ) {            
				$(_this.dom.navi_li).removeClass("seleced");
				$(_this.dom.sub).css("display", "none");
            } else {
		
				// get current ul index
				var index = $(this).index();
				
				//display ul with same index from sub
				$(_this.dom.sub_li).hide();
				$(_this.dom.sub_li).eq(index).css("display", "inline-block");
				
				//display sub ul
				$(_this.dom.sub).show();
				
				//change li style 
				$(_this.dom.navi_li).removeClass("seleced");
				$(this).addClass("seleced");
			}
			
		})
		
	},
}

Views.init = function(){
	Views.Table.init();
	Views.Filters.init();
}

/** Result table dict
*/
Views.Table = {

	dom : {
			table : "table",
			tbody : "table tbody",
			item_count : ".main-content__panel__item strong",
			shoping_cart : ".shoping-cart",
			cart_popup : ".cart-popup",
			cart_popup_close : ".cart-popup > a",
			cart_popup_add : ".cart-popup .add",
			cart_popup_configure : ".cart-popup .configure",
			cart_popup_checkout : ".cart-popup .checkout",
		},
	
	/** Initial function
	*/
	init : function () {
		this.Filter.init();
		this.Search.init();
		this.Show.init();
		this.Sort.init();
		this.JumpTo.init();
		this.Pagination.init();
		
		var _this = this;
		
		$(document).click( function( event ) {
		
			// hide on click away
			if( $(event.target).parents().index( $( _this.dom.cart_popup ) ) == -1 
			&& 	$( _this.dom.cart_popup ).is(":visible") )
				$( _this.dom.cart_popup ).hide();
			
			// display cart popup menu 
			//if( $(event.target).parent().is( _this.dom.shoping_cart ) ){
			//	$( _this.dom.cart_popup ).show();
			//	$( _this.dom.cart_popup ).css( { top :  $(event.target).position().top - 15 + "px" } );
			//}
		});
		
		// hide on esc
		$(document).keyup( function( event ) {
			if( $( _this.dom.cart_popup ).is(":visible") && event.which == 27 )
				$( _this.dom.cart_popup ).hide();
		});
		
		// hide popup on click 'x'
		$( this.dom.cart_popup_close ).click( function () {
			$( _this.dom.cart_popup ).hide();
		});
		
		// checkout click
		$( this.dom.cart_popup_checkout ).click( function () {
			$( _this.dom.cart_popup ).hide();
		});
		
		// add click
		$( this.dom.cart_popup_add ).click( function () {
			$( _this.dom.cart_popup ).hide();
		});
		
		// configure click
		$( this.dom.cart_popup_configure ).click( function () {
			$( _this.dom.cart_popup ).hide();
		});
	},
	
	displayItemCount : function( count ){
		$( this.dom.item_count ).text( count + " Servers Found" );
	},
	
	/** Fill table with data
		@data data array 
	*/
	draw : function( data ){
		//clear table
		$( this.dom.tbody ).html("");
		
		//hide cart popup
		$( this.dom.cart_popup ).hide();
		
		//element templates
		var row = $("<tr></tr>");
		var cell = $("<td></td>");
		var strong = $("<strong></strong>");
		var a = $("<a></a>").attr( {"href" : "#"} );
		var span = $("<span></span>").addClass("flag");
		var img = $("<img>").attr("alt", "flag");
                                        
		// Create cell with strong A element
		cellStrongA = function ( _data, _row, extra_class ) {
			var _cell = cell.clone().addClass( extra_class ).appendTo( _row );
			var _strong = strong.clone().appendTo( _cell );
			var _a = a.clone().html( _data ).appendTo( _strong );
			return _a;
		}
		
		if ( typeof(data) === "undefined" || data.length == 0 ) {
            $('<tr><td colspan="10" style="text-align:center;font-size:22px;"><p style="margin:40px;">Oops, seems there is no results!</br>Please remove some filters</p></td></tr>').appendTo(this.dom.tbody);
        }
		
		var row_count = 1;
				
		for ( r in data ){
			var _row = row.clone()
			
			if ( row_count % 2 ) {
				_row.css('background','#E3E3E3');
			}
			
			//Hot Deal
			_row.addClass( data[r][1] );
			
			//Brand
			//cell.clone().html( data[r][0] ).appendTo( _row );
			
			//CPU
			var _a = cellStrongA( data[r][2], _row, "cell-200-px" );
			_a.attr("href", data[r][10]);
			
			//CPU Score
			var _a = cellStrongA( data[r][3], _row, "center" );
			_a.attr({
				"href": data[r][11],
				'target' : '_blank'
			});			
			
			//Ram			
			var c = cell.clone().addClass("center cell-100-px").appendTo( _row );			
			var p = $('<p>').text( data[r][4][0] ).appendTo(c);
						
			if ( data[r][4][1].length > 0 ) {
				var q = $('<div>').addClass("question").appendTo(p);
				var qi = $('<div>').addClass("question__icon").text('?').appendTo(q);
				var qd = $('<div>').addClass("question__description").appendTo(q);
				var qdi = $('<div>').addClass("question__description__inner").text( data[r][4][1] ).appendTo(qd);
				var qdc = $('<div>').addClass("question__description__corm").text('&nbsp;').appendTo(qd);
			}
			
			//HDD
			var hdd_cell = cell.clone().addClass("center cell-100-px").appendTo( _row );
			for( var i=0; i<data[r][5].length; i++){
				hdd_cell.html( hdd_cell.html() + "<p>"+data[r][5][i]+"</p>" );
			}
			
			//Traffic
			cell.clone().addClass("center").html( data[r][6] ).appendTo( _row );
			
			//IP
			var ip_cell = cell.clone().addClass("center").appendTo( _row );
			for( var i=0; i<data[r][7].length; i++){
				ip_cell.html( ip_cell.html() + "<p>"+data[r][7][i]+"</p>" );
			}			
			
			//Location
			var _cell = cell.clone().addClass("center").appendTo( _row );			
			var countries = data[r][8].split(",");
			
						
			for ( var i = 0 ; i < countries.length; i++ ) {
				if( countries[i].length == 0 ) continue;
				//$("<p></p>").html( countries[i] ).appendTo( _cell );
				var p = $('<p style="text-transform:uppercase;"></p>').html(countries[i] + "  ");
				img.clone().attr(
					"src",
					"/wp-content/plugins/awesome-jquery-filters/assets/images/flags_b/" + countries[i] + ".png"
				).appendTo( p );
				p.appendTo( _cell );
			}
            
			//Price
			var _cell = cell.clone().appendTo( _row );
			strong.clone().html( CURRENCY_SYMBOL + data[r][9] + "/mo" ).appendTo( _cell );
			
			//var _html = _cell.html();
			//_cell.html( _html + data[r][11] );
			
			//Cart
			cell.clone().addClass("center"). html(
			//_cell.addClass("center").html(
			    '<ul class="table__list">\
                        <li>\
                            <a href="'+ data[r][10] +'" class="shoping-cart">\
								<div>buy</div>\
                                <!--img src="/wp-content/plugins/awesome-jquery-filters/assets/images/bg/shopping-cart.png" alt="shopping-cart" /-->\
							</a>\
						</li>\
					</ul>\
				</td>').appendTo( _row );  
			_row.appendTo( this.dom.tbody );
			
			row_count ++;
		}
	},
}
/** Table filter by deals & clearance
*/
Views.Table.Filter = {
	
	data : new Core.Data( 
		"table_filter", 
		function( data ){
			var _this = Views.Table.Filter;
			var el = $( _this.dom.filter_li + "[id='" + data + "']");
			
			if( el.length > 0 )
				_this._makeActive( el );
	
		}
	),
	
	dom : {
		filter : ".main-content__panel__navi",
		filter_li : ".main-content__panel__navi ul li",
	},
	init : function () {
		
		var _this = this;
		
		// on click filter
		$( this.dom.filter_li ).click( function(){
			if ( $(this).hasClass("selected") ) return;
			
			//work with elements classes
			_this._makeActive( $(this) );
			
			// get param id 
			var value = $(this).attr("id");
			
			// change filter Data
			_this.data.set( value );
			
		})
	},
	/** Make current element as active
		@el jquery object
	*/
	_makeActive : function( el ){
		$( Views.Table.Filter.dom.filter_li ).each( function(){
			$(this).removeClass("selected");
		});
		
		el.addClass("selected");
	}
}

/** Table search
*/
Views.Table.Search = {
	
    dom : {
        select : ".table-search",
        tag_list : ".table-search__tags",
        tags : ".table-search__tags li",
        text_edit : ".table-search__text"
    },
    
	text_max_size : 75,
	compact_row_size : 25,
	text_current_size : 0,
	
	_after_init : true,
	can_delete_tag : false,
	
	use_search : false,
	
    init : function () {		
		
		// Table Search		
					    
        var _this = this;
		
		$( _this.dom.select ).tagEditor({			
			autocomplete: {
				delay		: 0, // show suggestions immediately				
				source		: '/wp-admin/admin-ajax.php?action=getSuggestions', // '/wp-content/plugins/awesome-jquery-filters/assets/js/search.json',
				minLength	: 0				
			},			
			deleteOnBackspace : true,
			animateDelete : 0,
			onChange	: function(field, editor, tags) {
								
				if (Views.f.table_search.bool_clear_all) return;
				
				var string = (tags.length ? tags.join(',') : '');				
				Views.f.table_search.bunchSet( tags ); 
				Views.f.table_search.data.set( string );				                     				
			},
			forceLowercase : false
		});
		
		$('.tag-editor').css( 'width',
			$('.main-content__panel').width() - $('.main-content__panel__item:first').width() -
			$('.main-content__panel__item:nth-child(2)').width() - 30 + 'px' );
			
    }
}

Views.Table.Show = {

	data : new Core.Data( "show" ),
	
	dom : {
		select : "select[for='show']",
	},
	
	init : function () {

		var _this = this;
		
		_this.data.draw = _this.draw;
		
		$( _this.dom.select ).change( function(){
			_this.data.set( $(this).find("option:selected").val() );
		})
	},
	
	
	draw : function ( data ) {
		_this = Views.Table.Show;
		
		if( data.length == 0 ){
			$( _this.dom.select + " :first" ).attr("selected", "selected");
		} else {
			$( _this.dom.select + " option").each( function() {
				if( $(this).text() == data )
					$(this).attr("selected", "selected");
			});
		}
	},
}

Views.Table.Sort = {

	data : new Core.Data( 
		"sort",
		function ( data ) {
			_this = Views.Table.Sort;
			
			var data = data.split(',');
			
			if( data.length != 2)
				return;
			
			var el = $( _this.dom.span + "[id=" + data[0] + "]");
			
			if( el.length == 0 || $.inArray( data[1], ["up", "down"] ) == -1 )
				return;
			
			$( _this.dom.span ).removeClass("__arrow_up").removeClass("__arrow_down");
			el.addClass( "__arrow_" + data[1] );
		}		
	),
	
	dom : {
		span : "span.sortable",
	},
	
	init : function () {

		var _this = this;
		
		
		$( _this.dom.span ).click( function(){
				
			var order = 'down';
			
			if( $( this ).hasClass("__arrow_down") ){
				
				$(this).removeClass(" __arrow_down");
				$(this).addClass("__arrow_up");
				order = 'up';				
				
			}else{
				if( ! $( this ).hasClass("__arrow_up") ){
					$( _this.dom.span ).removeClass("__arrow_up").removeClass("__arrow_down");
				}
			
				$(this).addClass("__arrow_down");
			}
			
			var name = $(this).attr("id"); 
			
			_this.data.set( name + "," + order );
		})
	},
}

Views.Table.JumpTo = {

	data : new Core.Data( 
		"page",
		function ( data ) {
			_this = Views.Table.JumpTo;
			
			if( data.length > 0 && data <= Core.storage.page_count )
				$(_this.dom.input ).val( data );
		}		
	),
	
	dom : {
		input : "input#jump",
	},
	
	init : function () {

		var _this = this;
		
		// confirm by press enter while focus 
		$( _this.dom.input ).focus( function(){
		
			$( _this.dom.input ).on( "keydown", function( event ){
				
				$( _this.dom.input ).css('color' , 'black');
				
				if ( event.which == 13 ){
					
					var _val = $( _this.dom.input ).val();
					
					// if a number
					if( ! isNaN (_val) && _val != 0 && _val > 0  ){
					
						// set last page if current page gt last
						if(  _val > Core.storage.page_count ){
							$( _this.dom.input ).val( Core.storage.page_count );
							_val = Core.storage.page_count;
						}
						
						_this.data.set( _val );
					} else {
						$( _this.dom.input ).css('color', 'red');
					}
				}
			})
		})
	},
}

Views.Table.Pagination = {

	data : new Core.Data( 
		"page",
		function ( data ) {
			_this = Views.Table.Pagination;
			
			// dev: update info in global storage			
			if( data.length > 0 && data <= Core.storage.page_count ){
				var matches = data.match(/\d+/g);
				if (matches == null) {
					Core.storage.page = 1;
				} else {
					Core.storage.page = parseInt( data );
				}
			}
			
			//reinitialize pagination
			_this.init();
		
		}		
	),
	dom : {
		ul : "ul#paginator",
		a : "ul#paginator li a",
		prev : ".page-navi__prev a",
		next : ".page-navi__next a",
	},
	
	init : function () {

		var _this = this;
		var max = 8; 
		var end = Core.storage.page_count;
		var page = Core.storage.page;
		
		//clear paginator
		$(_this.dom.ul).html("");
		
		//add new elements to paginator
		$('<li class="page-navi__prev"><a href="#"><span>prev arrow</span></a></li>').appendTo( $(_this.dom.ul) );
		
		if( end <= 10 ){
			for ( var i = 1; i <= end; i++ ){
				_this._addElement( i );
			}
		} else {
			
			// first triplet lenght 
			var first_start = 1,
				first_end = 3,
				first = [1]
			;
			
			if( end - page < 3 ) { 
				first_end -= end - page;
		
				for ( var i = first_start ; i < first_end; i ++ ){
					first.push( i + 1 );
				}
			}
			
			// triplet before element
			var before = [];
			var start_before = page - 3;
			
			if( start_before <= first[ first.length - 1 ] )
				start_before = first[ first.length - 1 ] + 1
			
			for( var i = start_before; i < page; i++ ){
				before.push( i );
			}
				
			// last triplet lenght 
			//< 1 ! 3 4 5 ... 18 19 20 >
			var last_start = end - 3,
				last_end = end,
				last = [ end ]
			;
			
			if( page - first_start < 3 ) { 
				last_start = end - 5 + page;
				
				for ( var i = last_end - 1 ; i > last_start; i -- ){
					last.unshift( i );
				}
			}
			
			// triplet after element
			var after = [];
			var after_end = page + 3;
			
			if( after_end >= last[0] )
				after_end = last[0] - 1;
			
			for( var i = page; i <= after_end; i++ ){
				after.push( i );
			}
			
			// draw elements
			if( first[0] != after[0] )
				_this._addElementsFromList( first );
			
			if( before[0] - first[ first.length - 1]  > 1 )
				$('<li>...</li>').appendTo( $(_this.dom.ul) );
			
			_this._addElementsFromList( before );
			_this._addElementsFromList( after );
			
			if( last[0] - after[after.length - 1] > 1 )
				$('<li>...</li>').appendTo( $(_this.dom.ul) );
				
			_this._addElementsFromList( last );	
					
		}
		// make bold			
		$(_this.dom.a).removeClass("selected-page");
		$(_this.dom.a).each( function(){
			if ( $(this).text() == page )
				$(this).addClass("selected-page");
		}) 
		
		$('<li class="page-navi__next"><a href="#"><span>next arrow</span></a></li>').appendTo( $(_this.dom.ul) );
		
		//change arrows color
		if( page == 1 ){
			$( this.dom.prev ).css({ "background-image" : 'url("../wp-content/plugins/awesome-jquery-filters/assets/images/icons/arrow-left-gray.png")', cursor : 'default' });
		}else{
			$( this.dom.prev ).css({ "background-image" : 'url("../wp-content/plugins/awesome-jquery-filters/assets/images/icons/arrow-left.png")', cursor : 'pointer' });

		}
		if( page == end ){
			$( this.dom.next ).css({ "background-image" : 'url("../wp-content/plugins/awesome-jquery-filters/assets/images/icons/arrow-right-gray.png")', cursor : 'default' });
		}else {
			$( this.dom.next ).css({ "background-image" : 'url("../wp-content/plugins/awesome-jquery-filters/assets/images/icons/arrow-right.png")', cursor : 'pointer' });
		}
		
		// bind events
		$( _this.dom.a ).click( function(){
			
			if( $(this).parent().hasClass('page-navi__prev') ){
				if( Core.storage.page - 1 > 0 )
					_this.data.set( Core.storage.page - 1 );
			} else {
				if ( $(this).parent().hasClass('page-navi__next') ) {
					if( Core.storage.page + 1 <= Core.storage.page_count )
						_this.data.set( Core.storage.page + 1 );
				} else {
					_this.data.set( $(this).text() );
				}
			}
			
		})
	},
	
	/** Add <li> element to <ul>
		@val - string
	*/
	_addElement : function ( val ) {
		$('<li><a href="#">' + val + '</a></li>').appendTo( $( this.dom.ul) );
	},
	/** Create <li> elements from list
		@list - array of strings
	*/
	_addElementsFromList : function ( list ) {
		for ( var i in list ){
			this._addElement( list[i] );
		}
	},
}

/** Base filter class
	@name string - name of the filter
	@group_name string - name of fiter group
*/
var Filter = function ( name, group_name ){
	
	this.name = name;
	
	this.sort_bunch = true; 			// default bunch alphabet sort
	this.group_name = group_name;
	this.bunch_visual_type = "list";	//default type
	this.clear = function () {};  		//default clear function
	this.realClear = function () {};  		//default clear function
	
	// store this obj in filter storage
	Views.filters[ name ] = this;
	
	// default value of input if none selected
	this.null_input_value = " Any";
	
	/** bunch workaround */
	this.bunchSet = function ( data ) {
		Views.FilterBunch.set( this, data );
	};
	this.bunchRemove = function () {
		Views.FilterBunch.remove( this );
	};
	
	// Dependence
	this.dependCheckFnList = [];
	
	/** default values activating */
	this.dependDefault = function () {
	};
	
	/** extra after change input function */
	this.afterChangeInput = function () {
	};
	
	this.getDependId = function () {};

	this.dependProccess = function ( filter, __this, el ){
	
		var _clear = function () {
				
			$( __this.depend_on.container_selector ).hide();
			__this.realClear( false );
			
		};
		
		// create and add after change fn to depend fn
		var _proccess = function( ){
			
			if( el.prop("checked") ){
				
				$( __this.depend_on.container_selector ).show();
				
				// run default values activating
				__this.dependDefault();

			} else {
				_clear();
			}
		};
				
		// proccess if any selected 
		if( __this.depend == "any" ){
			_proccess();
			return;
		}
		
		// for checkboxes without multiple selected
		if( typeof( filter.multiple_select ) !== 'undefined' )
			if( ! filter.multiple_select ) {
				if( el.attr("id") == __this.depend.attr("id") && el.prop("checked") )
					$( __this.depend_on.container_selector ).show();
				else	
					_clear();
			}
		
		// proccess if only data_id selected
		if( el.attr("id") == __this.depend.attr("id") )
			_proccess();
		
	};

	/** activate depend */
	this.dependActivate = function () {
			
		if( typeof( this["depend_on"] ) !== 'undefined' ){
		
			// set depend
			if( typeof( this.depend_on["data_id"] ) !== 'undefined' ){
				// depend on current value
				this.depend = this.depend_on.filter.getDepend( this.depend_on.data_id );
			} else {
				this.depend = "any";
			}
			
			// hide container			
			$( this.depend_on.container_selector ).hide();
			
			this.depend_on.filter.dependCheckFnList.push( [this.dependProccess, this] );
			
		} else {
			this.depend = false;
			this.dependCheckFnList = [];
		}
	};
}

/** Storage of filters obj
*/
Views.filters = {
		"filter_name" : "filterObj"
}

/** Filters namespace
*/
Views.f = {}


/** Subclass of top checkbox filters
*/
var TopFilter = function ( name ) {
	// required 
	this.name = name;	
	this.data = new function () {};
	this.reload = this.data; 
	
	Views.filters[ name ] = this;
	
	/** Check for 0 value, set default null value */
	this.setNullValueToInput = function ( size, selector ) {
		if( size > 0 )
			$( selector ).val(  size + " Selected" );
		else
			$( selector ).val(  this.null_input_value );
	}

}
TopFilter.prototype = new Filter( );

/** Top Slider default class
*/
var Slider = function( attrs ) {

	// required attributes
	required_attrs = ["name", "group_name"];
	
	//check for required attrs
	for( var i = 0; i < required_attrs.length; i++ ) {
		if( typeof( attrs[ required_attrs[i] ] )  === 'undefined' ) {
			dbg("Attribute " + required_attrs[i] + " not found!");
		}
	}
		
	// default values
	this.reload = this.data; 
	this.slider_options = {
		"steps" : []
	}
	this.dimensions = [];
	
	// set attributes from attrs
	for( var i in attrs )
		if( typeof( attrs[i] )  !== 'undefined' ) {
			this[i] = attrs[i];
		}
		
	if ( typeof(this.slider_options.range) !== 'undefined' ) {
		this.slider = $("div[data-slider-name='" + this.name + "']");
		this.slider.attr('data-slider-range', this.slider_options.range[0] + ',' + this.slider_options.range[1] );
	}
	if ( typeof(this.slider_options.steps) == 'undefined' ) {
		this.slider_options.steps = [];
	}

	// functions ===================================================================================================
	
	var _this = this;	
	
	this.init = function () {
		this.bunch_visual_type = "range";
		
		this.slider = $("div[data-slider-name='" + this.name + "']");
		
		// set input row
		if( typeof( this["input_selector"] ) !== 'undefined' )
			this.input = $( this["input_selector"] ); 
		else
			this.input = false;
		
		// set parent div 
		if( typeof( this["pop_up_container_selector"] ) !== 'undefined' )
			this.parent_container = $( this["pop_up_container_selector"] ); 
		else
			this.parent_container = false;
			
		// set range values		
		
		this.range = this.slider.attr('data-slider-range').split(',');
		this.range[0] = parseFloat( this.range[0] );  
		this.range[1] = parseFloat( this.range[1] );
		
		Views.filters[ this.name ] = this;
		
		this.target = this.slider.attr('data-slider-target').split(',');
		this.targetEl = {
			min: $('[data-slider="' + this.target[0] + '"]'),
			max: $('[data-slider="' + this.target[1] + '"]')
		};
		
		// set default types
		if( this.slider_options.steps.length > 0 ){
			this.types = [ this.slider_options.types[0], this.slider_options.types[1] ];
		}else{
			var type = this.slider.attr('data-slider-type') ? this.slider.attr('data-slider-type') : '';
			this.types = [ type, type ];
			this.type = type;
		}
	
		// set defaul value
		if( this.input ) 
			this.input.val( this.null_input_value );
				
		// add slider
		this.addSlider();
		
		if( this.parent_container ) {
		
			//on mouseleave slider div
			var father = this.slider.parent().parent().parent();
			
			$(father).parent().parent().on( "mouseleave", function () {
				if( _this._reload_on_mouseleave ) {
					$(father).hide();
					_this.afterChange();
				}
			});
			
			$(document)
				.keyup( function ( event ){
			
					if( ! $( _this.parent_container.find('.upper-filter__dropdown-menu') ).is(":visible") )
						return;
						
					// on esc
					if( event.which == 27 ){
						$( _this.parent_container.find('.upper-filter__dropdown-menu') ).hide();
						_this._reload_on_mouseleave = false;
						_this.data.redraw();
					}
					
					//on enter
					if( event.which == 13 ){
						$( _this.parent_container.find('.upper-filter__dropdown-menu') ).hide();
						_this.afterChange();
					}
				});
				
			this.input.click( function(){
					$('.upper-filter__dropdown-menu').hide();
					$( _this.parent_container.find('.upper-filter__dropdown-menu') ).show();
					_this._reload_on_mouseleave = true;
				});
		} 
		
		// set default values 
		this.resetSlider();
		
		//clear on 'x'
		if( this.input ) 
			this.input.parent().next().click( function () {
				_this.removeValue();
			});
		
	}
	
	// construct data object
	this.data = new Core.Data( this.name, function ( dt ) {
		var dt = dt.split(':'),
			_dt = _this.value
		;
		
		// check for values count
		if ( dt.length == 2){
			
			var numeric_check = true;
			// check for numbers
			for ( var i in dt ){
				if( ! $.isNumeric( dt[i] ) )
					numeric_check = false;
			}
			
			//check for value
			if( numeric_check ){
				dt = [ parseInt(dt[0]), parseInt(dt[1]) ];
				
				if( !( dt[0] > parseInt(_this.range[1]) || dt[0] < parseInt(_this.range[0]) 
				|| dt[0] > dt[1] || dt[1] > parseInt(_this.range[1]) ) ) {
					var _dt = dt;
					
					_this.setValues( dt, 'real');
					
					//change slider range values & input
					_this.changeSliderCurrentValues();
					_this.changeSliderToggleValues();
					
					if( _this.input ){
						if( _this.dimensions.length > 0 ) {
						
							_this.input.val( 
								_this.addDimensionToValue( _this.values[0].display, 'input' ) + 
								' - ' +
								_this.addDimensionToValue( _this.values[1].display, 'input' )
							);
							
						} else {
							_this.input.val( _this.values[0].display + _this.types[0] + " - " + _this.values[1].display + _this.types[1] );
						}
					}	
				}
				
				_this._bunchSet( );
			}
		}else{
			_this._bunchSet( false );
		}
		
		//check if current values = default, restore input defaul view
		if( _this.input )
			if ( _this.values[0].real == _this.range[0] && _this.values[1].real == _this.range[1] ) {
				_this.input.val( _this.null_input_value );
			}
	} );

	/** Convert display value to real */
	this._display2real = function ( data, type ) {
		
		var _data = data;
		
		if ( type == this.slider_options.types[1] ){
			_data = data * this.slider_options.convert_border;
		}
		
		return _data;
	}
	
	/** Convert internal value to real */
	this._internal2real = function ( data ) {

		// get real data value
		return this.slider_options.steps[ data ]; 
	}

	/** Convert real value to display
		@data - current value
		@return [ display_data, new_data_type ]
	*/
	this._real2display = function ( data ) {

		var display_data = data,
			zz = data / this.slider_options.convert_border,
			type_index = 0;
			
		if( zz >= 1 ){
			display_data = zz;
			type_index = 1; 
		}
			
		var type = this.slider_options.types[ type_index ];
			
		return [ display_data, type ]
	}

	/** Convert real value to internal */
	this._real2internal = function ( data ) {
		for ( var i = 0; i < this.slider_options.steps.length ; i++ ){
			if( this.slider_options.steps[i] == parseInt( data ) )
				return i;
		}
	}
		
	/** Set values in internal storage
		@data : [left toggle val, right toggle val]
		@type :	
			"internal", - slider real values (0....N) 
			"display", -- converted int value (like 1 TB)
			"real" - real int value (like 1000 GB)
	*/
	this.setValues = function ( data, data_type ){
	
		this.values = [{},{}];
		
		// set current values as default
		for( var i = 0; i < data.length; i++ ) {
		
			this.values[i].internal = data[i],
			this.values[i].real = data[i],
			this.values[i].display = data[i];
		}
		
		// check for non convert
		if( this.slider_options.steps.length == 0 )	
			return;
		
		// run convert functions
		for( var i = 0; i < data.length; i++ ) {
			try {
				switch( data_type ) {
					case "internal":
						this.values[i].real = this._internal2real( data[i] );
						
						var zz = this._real2display( this.values[i].real );
						this.values[i].display = zz[0];
						this.types[i] = zz[1];
						
						break;
						
					case "real":
						this.values[i].internal = this._real2internal( data[i] );
						
						var zz = this._real2display( data[i] );
						this.values[i].display = zz[0];
						this.types[i] = zz[1];
						
						break;
				}
			} catch (e) {
				// do nothing ...
			}
		}
	}

	/** change slider current left and right values */
	this.changeSliderToggleValues = function () {
		
		// if Dimension created
		if( this.dimensions.length > 0 ) {
			
			this.targetEl.min.html( 
				this.addDimensionToValue( this.values[0].display, 'slider' )
			);
			
			this.targetEl.max.html( 
				this.addDimensionToValue( this.values[1].display, 'slider' )
			);
			
			return;
		} 
			
		// old method
		this.targetEl.min.html( this.values[0].display + this.types[0] );
		this.targetEl.max.html( this.values[1].display + this.types[1] );
	}
	
	/** change slider real values */
	this.changeSliderCurrentValues = function () {
		this.slider.slider( "option", "values", [ this.values[0].internal, this.values[1].internal ] );
	}
	
	/** add slider function */
	this.addSlider = function () {
		
		// on change slider value function
		var onChange = function ( event, ui ) {
			_this.setValues( [ui.values[0], ui.values[1]], 'internal' );
			// set default slider length values
			_this.changeSliderToggleValues();
			
		};
		
		// on stop changing slider
		var onStop = function ( event, ui ) {
			// run after change in parent container not defined
			if( ! _this.parent_container )
				_this.afterChange();
		}
		
		$( ".selector" ).on( "slidestop", function( event, ui ) {} );
		
		// parse slider_options
		var slider_options = {
			min: parseInt( this.range[0] ),
			max: parseInt( this.range[1] ),
			values: [ 
				parseInt( this.range[0] ), 
				parseInt( this.range[1] ) 
			],
			range: true,
			slide: onChange,
			change: onChange,
			stop: onStop,
		};
		if( this.slider_options.steps.length > 0 ) { 
				slider_options.step = 1;
				slider_options.min = 0;
				slider_options.max = this.slider_options.steps.length - 1;
				slider_options.values = [0, slider_options.max];
			}
		
		this._slider_options = slider_options;
		// add slide event
		this.slider.slider( this._slider_options );
	}
		
	/** Actions after change some value */
	this.afterChange = function () {
		
		if ( this.input ) {
			if( this.input.val() == this.null_input_value )
				if( this.values[0].real == this.range[0] && this.values[1].real == this.range[1] )
					return;
		} 
		
		if( this.input ){
			if( this.dimensions.length > 0 ) {
			
				this.input.val( 
					this.addDimensionToValue( this.values[0].display, 'input' ) + 
					' - ' +
					this.addDimensionToValue( this.values[1].display, 'input' )
				);
				
			} else {
				this.input.val( this.values[0].display + this.types[0] + " - " + this.values[1].display + this.types[1] );
			}
		}
			
		if( this.values[0].real == this.range[0] && this.values[1].real == this.range[1] ) {
			this.data.remove();
			return;
		}	
					
		this._bunchSet( );
		this.data.set( this.values[0].real + ":" + this.values[1].real );
		
	};
	
	/** real bunch set method 
		@param send_data - true or false (send data or none)
	*/
	this._bunchSet = function ( send_data ) {
		
		if( typeof( send_data ) !== 'undefined' ){
			this.bunchSet();
			return;
		}
		
		if( this.dimensions.length > 0 ) {
		
			this.bunchSet( [
				this.addDimensionToValue( this.values[0].display, 'applied_in_input' ),
				this.addDimensionToValue( this.values[1].display, 'applied_in_input' )
			] );
			
			return;
		}
		
		// enable multiple types support
		if( this.slider_options.steps.length > 0 ) { 
			this.bunchSet( [ this.values[0].display + this.types[0], this.values[1].display + this.types[1] ] );
		} else {
			this.bunchSet( [ this.values[0].display, this.values[1].display ] );
		}
		
	}
	
	/** after clear all reset input value */
	this.clear1 = function () {
	
		if( this.input )
			this.input.val( this.null_input_value );
			
		this.resetSlider();
	}
	
	/** Set default values, slider current and toggles values */
	this.resetSlider = function () {
		// set default values 
		this.values = [];
		this.setValues( [ this.range[0], this.range[1] ], 'real' );
		
		// set default slider length values
		this.changeSliderCurrentValues();
		this.changeSliderToggleValues();
	}
	
	/** Remove one value and make afterChange */
	this.removeValue = function ( ) {
		this.clear1();
		
		this.bunchRemove();
		this.data.remove();
	}

	/** for pair value1 and value2 get nearly values from data[...]*/
	this._getNearlyValues = function ( fl1, fl2 ) {
		for( var i = 1; i < this.slider_options.steps.length; i++ ) {
			if( this.slider_options.steps[i] > fl1 ){
				var fl1 = this.slider_options.steps[i - 1];
				break;
			}
		}
		for( var i = this.slider_options.steps.length - 2; i >= 0; i-- ) {
			if( this.slider_options.steps[i] < fl2 ){
				var fl2 = this.slider_options.steps[i + 1];
				break;
			}
		}
		return [fl1, fl2];
	};
	
	/**  on change from bunch */
	this.onChangeFromBunch = function ( dt ) {
		
		
		var err = true;
		var _dt = [];
		var changeType = function(){};
		
		// enable multiple types support		
		if( this.slider_options.steps.length > 0 ) {

			var type_string = "";
			
			for( var i = 0 ; i < this.slider_options.types.length ; i++ ){
				if( type_string.length > 0 )
					type_string += "|"; 
					
				type_string += this.slider_options.types[i];
			}
		}
		
		// create regexp
		if( typeof( type_string ) !== 'undefined' )
			var re = new RegExp( '(^\\d+(?:\\.\\d+)?)(' + type_string + ')?' , 'i');
		else
			var re = new RegExp( '(^\\d+(?:\\.\d+)?)' , 'i');
				
		// check for int values
		if ( dt.length == 2 ) {
		
			// cut all available dimensions from string 
			for( var ii in dt ){
				if( this.dimensions.length > 0 ) {
					for( s in this.dimensions ) {
						dt[ii] = dt[ii].replace( this.dimensions[s].value, '' );
					}
				}
			}
			
			var re1 = re.exec( dt[0] );
			var re2 = re.exec( dt[1] );
			
			// check for correct value
			if ( re1 != null && re2 != null ) {
				var fl1 = parseFloat( re1[ 1 ] );
				var fl2 = parseFloat( re2[ 1 ] );
				
				// convert type
				if( this.slider_options.steps.length > 0 ) {
				
					// check for correct type in value
					if( typeof( re1[2] ) !== 'undefined' && typeof( re2[2] ) !== 'undefined' ) {
			
					
						fl1 = this._display2real( fl1, re1[2] );
						fl2 = this._display2real( fl2, re2[2] );
					
						// replace value by nearest real value 
						fl1 = this._getNearlyValues( fl1, fl2 )[0];
						fl2 = this._getNearlyValues( fl1, fl2 )[1];
				
					} else {
						
						// if values > convert border
						if ( fl1 >= this.slider_options.convert_border || fl2 >= this.slider_options.convert_border ) {
						
							// use GB
							if ( fl1 >= this.slider_options.convert_border ) {
								fl1 = this._getNearlyValues( fl1, fl2 )[0];
								this.types[0] = this.slider_options.types[0];
							} else {
								fl2 = this._getNearlyValues( fl1, fl2 )[1];
								this.types[1] = this.slider_options.types[1];
							}
							
						} else {
							
							// try to convert 
							var fl1_converted = fl1*this.slider_options.convert_border;
							var fl2_converted = fl2*this.slider_options.convert_border;
							
							// check value 1
							if( fl1_converted < this.slider_options.steps[ this.slider_options.steps.length - 1 ] ) {
									fl1 = this._getNearlyValues( fl1_converted, fl2 )[0];
									var changeType = function( _this ){_this.types[0] = _this.slider_options.types[1];};
							} else {
								fl1 = this._getNearlyValues( fl1, fl2 )[0];
								var changeType = function( _this ){_this.types[0] = _this.slider_options.types[0];};
							}
							
							// check value 2
							if( fl2_converted < this.slider_options.steps[ this.slider_options.steps.length - 1 ] ) {
								fl2 = this._getNearlyValues( fl1, fl2_converted )[1];
								var changeType = function( _this ){_this.types[1] = _this.slider_options.types[1];};
								
							} else {
								fl2 = this._getNearlyValues( fl1, fl2 )[1];
								var changeType = function( _this ){ _this.types[1] = _this.slider_options.types[0]; };
							}
						}
					}
				}
				
				//check for correct value
				if( this.range[0] <= fl1 && fl1 <= fl2 && fl2 <= this.range[1] ){
					_dt = [ fl1, fl2 ];
					changeType( this );
					err = false;
				}
			}
			
		}
		
		// check for err and complete
		if ( err ) {
			this._bunchSet();
		} else {
			this.setValues( _dt, 'real' );
			this.afterChange();
		};
		
	}

	/** Add dimension to value
		@param value 
		@param display position ("input", 'slider', 'applied')
	*/
	this.addDimensionToValue = function ( value, display_position ) {
		
		var _value = value;
		
		for( var s in this.dimensions ) { 
			if( this.dimensions[s].display_in[ display_position ] ) {
				if( this.dimensions[s].position == 0 )
					_value = this.dimensions[s].value + _value;
				if( this.dimensions[s].position == 1 )
					_value += this.dimensions[s].value;	
			}
		}
		
		return _value;
	};
	
	/** Get dimension to the bunch span */
	this.getBunchSpanDimension = function () {
		for( var s in this.dimensions ) { 
			if( this.dimensions[s].display_in.applied_after_input ) {
					return this.dimensions[s].value;
			}
		}
		
		return '';
	}
	
	// initialize current slider
	this.init()
}

Slider.prototype = new Filter( );


var Dimension = function ( attrs ) {

	this.name = "Dimension";
	this.required_attrs = ["value"];
	
	this.display_in = {
		slider : true, // display on slider f.e |-----------| 15$
		applied_in_input : false, 
		applied_after_input : true,
		input : true, // display in input f.e [ 15$ ] 
	};
			
	this.convertable = false;

	this.position = 1; // 0 - before element, 1 - after element
	
	this.init( attrs );	
}

Dimension.prototype = Core.Class;

/** Left panel with filters
*/
Views.FilterBunch = {

	dom: {
		sidebar : ".sidebar",
		filter : ".sidebar .filter",
		clear_link : ".sidebar .clear-link", 
	},
	
	groups : {},
	
	init : function () {
		
		var _this = this;
		
		// clear all button event
		$( this.dom.clear_link ).unbind().click( function () {
			
			// hide all groups and flush group filters
			for( var i in _this.groups ){
				var gr = $(".filter__item[filter-group='" + _this.groups[ i ].name + "']");
				gr.hide();
				_this.groups[ i ].flushFilter();
				_this.groups[ i ].filterItemSizeCheck();
			}
			
			// remove all filters data without reload
			for( var i in Views.filters ){
				if( typeof( Views.filters[i]["data"] ) !== 'undefined' )
					Views.filters[i].data.remove( false );
				
				if( typeof( Views.filters[i]["clear1"] ) !== 'undefined' )
					Views.filters[i].clear1( );
			}
						
			// update data
			setTimeout(function(){
				Core.update();
			},50);					
		});
		
	},
	
	/** add FilterObj to bunch */
	set : function ( filterObj, data ) {
		
		//check if group not exist and create it
		if( typeof( this.groups[ filterObj.group_name ] ) === 'undefined' ) {
			this.groups[ filterObj.group_name ] = new Views.FilterBunch.Group( filterObj.group_name ); 
		}
		
		var group = this.groups[ filterObj.group_name ];
		
		//add data
		group.addFilter( filterObj, data );
		
		// show filter if items > 0
		group.filterItemSizeCheck();
	},
	
	/** remove filter value from bunch */
	remove : function ( filterObj ) {
		if( typeof( this.groups[ filterObj.group_name ] ) !== 'undefined' ) {
			this.groups[ filterObj.group_name ].flushFilter( filterObj );
			this.groups[ filterObj.group_name ].filter_item.hide();
		}
	},

}

/** Visual group of filters in left panel (bunch)
*/
Views.FilterBunch.Group = function ( name, help_message ){
	
	this.name = name;
	this.help_message = "";
	this.filter = new function (){};
	
	if( typeof( help_message ) !== 'undefined' )
		this.help_message = help_message;
	
	// create el
	this.filter_item = $("<li></li>").addClass("filter__item").attr( "filter-group" , this.name).appendTo( $( Views.FilterBunch.dom.filter ) );
	var fp = $("<div></div>").addClass("filter__panel").appendTo( this.filter_item );
	var ft = $('<div></div>').addClass("filter__title").appendTo( fp );
	$("<h3>" + this.name + "</h3>").appendTo( ft );
	
	// add help message text
	if( this.help_message.length > 0 ){
		var q = $('<div></div>').addClass("question").appendTo( this.filter_item );
		$('<div>?</div>').addClass("question__icon").appendTo( q );
		
		var qd = $('<div></div>').addClass("question__description").appendTo( q );
		$('<div>' + this.help_message + '</div>').addClass("question__description__inner").appendTo( qd );
		$('<div>&nbsp;</div>').addClass("question__description__corm").appendTo( qd );
	}
	
	// add clear link and filter__list
	var a = $("<a>clear</a>").attr("href", "#").addClass('clear-link').appendTo( fp );
	this.filter_list = $("<ul></ul>").addClass("filter__list").appendTo( this.filter_item );
	
	var _this = this;
	
	// on remove by click "x"
	a.unbind().click( function () {
	
		_this.flushFilter();
		_this.filter_item.hide();
		
		// remove all filters data without reload
		for( var i in Views.filters ){
			var filter = Views.filters[i];
			
			if( filter.group_name == _this.name ) {
				filter.data.remove( false );
				
				if( typeof( filter["clear1"] ) !== 'undefined' )
					filter.clear1( );
			}
		}
		
		Core.update();
	});
	
	/** sort hash by key */
	this._sortHashByKey = function ( hash ) {
		
		var tuples = [],
			_hash = {};

		for (var key in hash) tuples.push([key, hash[key]]);

		tuples.sort(function(a, b) {
			a = a[0].toLowerCase();
			b = b[0].toLowerCase();

			return a < b ? -1 : (a > b ? 1 : 0);
		});
		
		for (var i = 0; i < tuples.length; i++) {
			var key = tuples[i][0];
			var value = tuples[i][1];
			
			_hash[ key ] = value;
		}
		
		return _hash;
	}
	
	/** revert hash */
	this._revertHash = function ( hash ) {
	
		var _hash = {};
		
		for ( i in hash ){
			_hash[ hash[i] ] = i;
		}
		
		return _hash;
	}
	
	/** add filter value to group */
	this.addFilter = function ( filterObj, data ) {
		
		// flush current filter value in group
		this.flushFilter( filterObj );
		
		// add value	
		switch ( filterObj.bunch_visual_type ) {
			case "list":
				
				// revert data (need to correct sort by string key)
				var data = this._revertHash( data );
				
				// sort data by key
				if( filterObj.sort_bunch ) 
					data = this._sortHashByKey( data );
				
				// create <li> elements				
				for( var i in data ){

					var fl_li = $("<li></li>")
						.attr( {"filter-name": filterObj.name, "filter-id" : data[i] } )
						.appendTo( this.filter_list );
						
					$("<a></a>").attr("href", "#").text( i ).appendTo( fl_li );
				}				
				break;
				
			case "range":
			
				// don't create <li>
				if( typeof( data ) === 'undefined' ) 
					break;
				
				var fl_li = $("<li></li>").attr( {"filter-name": filterObj.name} )
					.appendTo( this.filter_list );
				$("<a></a>").attr("href", "#").text( data[i] ).appendTo( fl_li );
				
				var fn = $("<div></div>").addClass("filter__number").appendTo( fl_li );
				var in1 = $("<input>").val( data[0] ).appendTo( fn );
				$("<span>to</span>").appendTo( fn );
				var in2 = $("<input>").val( data[1] ).appendTo( fn );
				
				// add data type
				if( filterObj.dimensions.length > 0 ) {
					$("<span>").html( filterObj.getBunchSpanDimension() ).appendTo( fn );
					
				} else {
					$("<span>").html( filterObj.type ).appendTo( fn );
				}
				
				filterObj.confirm_once = false;
				
				// confirm by press enter while focus 
				fn.find("input").focus( function(){
					
					// remove dimensions 
					for ( var i in filterObj.dimensions ){
						$(this).val( $(this).val().replace( filterObj.dimensions[i].value, '') );										
					}
					for ( var i in filterObj.types ){
						$(this).val( $(this).val().replace( filterObj.types[i], '') );										
					}
					
					filterObj.confirm_once = false;
					
					// confirm by press enter
					$( this ).on( "keydown", function( event ){
						if ( event.which == 13 ){
							filterObj.confirm_once = true;
							filterObj.onChangeFromBunch( [in1.val(), in2.val()] );
						}
					})
				})
				
				// confirm by focus out
				fn.find("input").focusout( function(){
				
					// prevent double conformation
					if( ! filterObj.confirm_once )
						filterObj.onChangeFromBunch( [in1.val(), in2.val()] );
					
					fn.find("input").off( "keydown" );			
				})
		
				break;
		}
		
		// remove by click "x"
		_this.filter_list.find("a").unbind().click( function () {
			var filter_name = $(this).parent().attr("filter-name");
			
			if( typeof ( Views.filters[ filter_name ] ) === 'undefined' )
				return;
			
			switch ( filterObj.bunch_visual_type ) {
				case "list":			
					Views.filters[ filter_name ].removeValue( $(this).parent().attr("filter-id") );
					break;
				case "range":
					Views.filters[ filter_name ].removeValue(  );
			}
		});

	};
	
	/** flush current filter data, hide group */
	this.flushFilter = function ( _filter ) {
		
		if( typeof( _filter ) === 'undefined' ){
			this.filter_list.find("li").remove();
			return;
		}
		
		this.filter_list.find("li[filter-name='" + _filter.name + "']").remove();
	};
	
	/** check for filter item count, show or hide group, remove from data */
	this.filterItemSizeCheck = function ( ) {
		if( this.filter_list.find("li").size() > 0 )
			this.filter_item.show();
		else
			this.filter_item.hide();
	};
}

Views.FilterBunch.init();

/** Checkbox filter default class

	in html define as:
		<input id="intel" type="checkbox" radio-fIlter-name="manufacture" />
		<label for="intel" >intel</label>

*/
var Checkbox = function( attrs ) {

	// required attributes
	required_attrs = ["name", "group_name"];
	
	//check for required attrs
	for( var i = 0; i < required_attrs.length; i++ ) {
		if( typeof( attrs[ required_attrs[i] ] )  === 'undefined' ) {
			dbg("Attribute " + required_attrs[i] + " not found!");
		}
	}
		
	// default values
	this.reload = this.data; 
	this.multiple_select = false;
	
	// set attributes from attrs
	for( var i in attrs )
		this[i] = attrs[i];
	
	// select by this attribute
	var selector_attribute = "radio-filter-name";
	var selector_attribute_value = "name";
	
	this.dom = {
		inputs : "input[" + selector_attribute + "='" + this[ selector_attribute_value ] + "']",
	};
	
	// functions ===================================================================================================
	
	var _this = this;	
	
	/** Initialize function */
	this.init = function () {
		
		this.inputs = $( "input[" + selector_attribute + "='" + this[ selector_attribute_value ] + "']" ); 
		
		// activate dependence
		this.dependActivate();
		
		// add this filter to filter list
		Views.filters[ this.name ] = this;
		
		// on change inputs
		$( this.inputs ).on( "change", function () {
			
			// run depend functions
			for( var i in _this.dependCheckFnList ){
				_this.dependCheckFnList[i][0]( _this, _this.dependCheckFnList[i][1], $(this) );
			}
			
			// proccess
			if( $(this).prop("checked") ){
			
				// single select
				if( ! _this.multiple_select )
					_this.inputs.prop("checked", false);
					
				$(this).prop("checked", true);	
			}
			
			// after change function call
			_this.afterChangeInput();
			_this.afterChange();
		});
				
	}
	
	// construct data object
	if ( typeof( attrs.data ) !== 'undefined' )
		this.data = attrs.data;    
	else
		this.data = new Core.Data( this.name, function ( dt ) {
			
			_this.inputs.prop("checked", false);
			
			var _dt = dt.split(',');
			var bd = {};
			
			_this.inputs.each( function () {
				
				for( var i in _dt )
					if( $(this).val() == _dt[i] ){
						
						// activate checkbox
						$(this).prop("checked", true);
						
						// add bunch
						var a = $(this).val() ;
						bd[ a ] = $(this).next().text();
						
						// run depend functions
						for( var j in _this.dependCheckFnList ){
							_this.dependCheckFnList[j][0]( _this, _this.dependCheckFnList[j][1], $(this) );
						}
						
						continue;
					}
				
			});
			
			_this.bunchSet( bd );
		});
	
	this.getDepend = function ( data ){
		return $(this.dom.inputs + "[value='" + data + "']"); //this.search_options.data[ data ].replace(/ /g,'' );
	};
	
	this.realClear = function ( reload ){
		this.clear( reload );
	};
	
	this.afterChangeInput = function () {};
	
	/** Actions after change some value */
	if ( typeof( attrs.afterChange ) !== 'undefined' )
		this.afterChange = attrs.afterChange;    
	else
		this.afterChange = function () {
		
		var bunch_dict = {},
			string = "";
		
		this.inputs.each( function () {
			if( $(this).prop("checked") ) {
				bunch_dict[ $(this).val() ] = $(this).next().text(); 
				
				if( string.length > 0)
					string += ',';
					
				string += $(this).val();
			}
		});
		
		this.bunchSet( bunch_dict );
		this.data.set( string );
		
	};
	
	/** Remove all values and make afterChange */
	if ( typeof( attrs.clear ) !== 'undefined' )
		this.removeValue = attrs.clear;    
	else
		this.clear = function ( reload ) {

		$( this.inputs ).prop("checked", false);
		this.bunchRemove();
		this.data.remove( reload );
	}
	
	this.clear1 = function(){
		// run depend functions
		for( var i in this.dependCheckFnList ){
			this.dependCheckFnList[i][1].clear( false ); 
			$(this.dependCheckFnList[i][1].depend_on.container_selector).hide();
		}
		this.clear( false ) ;
	};
	
	// removeValue
	if ( typeof( attrs.removeValue ) !== 'undefined' )
		this.removeValue = attrs.removeValue;    
	else
		this.removeValue = function ( checkbox_value ) {
		// unchecked input
		$( this.dom.inputs + "[value=" + checkbox_value +"]" ).prop("checked", false);
		
		// run depend functions
		for( var i in this.dependCheckFnList ){
			this.dependCheckFnList[i][1].clear( false ); 
			$(this.dependCheckFnList[i][1].depend_on.container_selector).hide();
		}
		
		// run afterChange command to reload  
		this.afterChangeInput();
		this.afterChange();
	}

	// initialize current slider
	this.init()
}

Checkbox.prototype = new Filter( );

/** List filter with search default class
	Views.f.cpu_fam = new SearchList ({
		name : "cpu_fam",
		group_name : "CPU",
		search_options : {
			1 : "Core i3",
			2 : "Core i5",
			3 : "Core i7",
			4 : "Xeon Xeon E3",
			5 : "Core i9",
			6 : "Core i11",
			7 : "Core i13"
		}	
	});
*/
var SearchList = function( attrs ) {

	// required attributes
	required_attrs = ["name", "group_name", "search_options"];
	
	//check for required attrs
	for( var i = 0; i < required_attrs.length; i++ ) {
		if( typeof( attrs[ required_attrs[i] ] )  === 'undefined' ) {
			dbg("Attribute " + required_attrs[i] + " not found!");
		}
	}
		
	// default values
	this.reload = this.data; 
	this.search_options = { 
		data : {}, 
		display_elements : 0,
	};
	this.selected = {};
	this._reload_on_mouseleave = false;
	this.templates = {
		li : '<li><input type="checkbox"/><label></label></li>'
	};
	
	// set attributes from attrs
	for( var i in attrs )
		this[i] = attrs[i];
	
	this.dom = {
		div : "." + this.name,
		input : "input[data-filter='" + this.name + "']",
		li : "." + this.name + " .under-navi__select__list li",
		ul : "." + this.name + " .under-navi__select__list",
		checkboxes : "." + this.name + " .under-navi__select__list input",
		search_clear : "." + this.name + " .under-navi__select__delete",
		search_input : "." + this.name + " .under-navi__select__search input", 
	}
	// functions ===================================================================================================
	
	var _this = this;	
		
	/** Initialize function */
	this.init = function () {
						
		// add filter instance to Views.filters storage
		Views.filters[ this.name ] = this;
		
		// activate dependence
		this.dependActivate();
		
		// fill ul with search values
		this._initialUlFilling();
		
		// convert search data to lower case
		
		this.search_options._lower_case_data = {};
		
		for( var i in this.search_options.data ) {
			_this.search_options._lower_case_data[i] = this.search_options.data[i].toLowerCase();
		}
		
		// close on mouseout & run update
		$(this.dom.div).parent().mouseleave(  function (){	
			_this._realConfirm( );
		})
			
		// change input value after change checkboxes
		$(document)
			.on( 'change', this.dom.checkboxes, function () {
				
				// add or delete from this.selected
				if( $(this).prop("checked") ){
					_this.selected[ $(this).val() ] = $(this).next().text();
				} else {
					if( typeof( _this.selected[ $(this).val() ] ) !== 'undefined' ) {
						delete _this.selected[  $(this).val() ];
					}
				}
				 
				// run depend functions
				for( var i in _this.dependCheckFnList ){
					_this.dependCheckFnList[i][0]( _this, _this.dependCheckFnList[i][1], $(this) );
				}
							
				// run real after change input method1
				_this.changeInput();
				
				_this._reload_on_mouseleave = true;
			})
			
			.on( 'keyup', function ( event ) {
		
				if( ! $( _this.dom.div ).is(":visible") )
					return;
					
				// clear on Esc
				if( event.which == 27 ){
				
					//hide
					$( _this.dom.div ).hide();
					_this._reload_on_mouseleave = true;
					_this.data.redraw();
				}
				
				// confirm on Enter
				if( event.which == 13 )
					_this._realConfirm();
			})
		;
		
		$( this.dom.input ).click( function () {
			$('.upper-filter__dropdown-menu').hide();
			$( _this.dom.div ).show();
		});
		
		// clear search on 'x'
		$(_this.dom.search_clear).click( function (){_this.realClear()} );
		
		// search algorythm
		$(this.dom.search_input).keyup( function () {
			
			var dt = {},
				ind = 0;
			
			for( var i in _this.search_options._lower_case_data ){
				
				if( ind >= _this.search_options.display_elements )
					break;
				
				// compare with lower case data
				if( _this.search_options._lower_case_data[i].indexOf( $(this).val().toLowerCase() ) !== -1 ){ 
					ind ++;
					// add not converted data
					dt[i] = _this.search_options.data[i];
				}
			}
			
			//remove all li elements
			_this._fillUlWithLi( dt );
		});
		
	}
	
	this.getDepend = function ( data ){
		var di = this.search_options.data[ data ].replace(/ /g,'' );
		return $( "#" + this.name + "__" + di );
	};
	
	/** real confirm method */
	this._realConfirm = function () {
			
		//hide
		$( this.dom.div ).hide();
		
		if( ! this._reload_on_mouseleave ) 
			return;
		
		this._reload_on_mouseleave = false;		
		
		// run extra function
		this.afterChangeInput();
		this.afterChange();
	}
	
	/** real clear method */
	this.realClear = function ( reload ) {
		$(this.dom.checkboxes + ":checked").prop("checked", false);
		$(this.dom.search_input).val( "" );
		this._initialUlFilling();
		
		//hide
		$( this.dom.div ).hide();
		
		this.bunchRemove();
		this.data.remove( reload );
	}
	
	/** Initial filling of ul*/
	this._initialUlFilling = function () {
		var i = 0,
		dt = {};
		
		for( var id in this.search_options.data ){
			if( i >= this.search_options.display_elements )
				break;
			i++;
			
			dt[id] = this.search_options.data[id];
		}
		
		this._fillUlWithLi( dt );
	}
	
	/** Rewrite default depend default data activator */
	this.dependDefault = function () {
		/*if( typeof( this.depend_on["default_id"] ) !== 'undefined'  ){
			$( this.dom.checkboxes + "[value='" + this.depend_on.default_id + "']" ).prop("checked", true);
			this.afterChange( false );
		}*/
	};
	
	/** Create li elements and append to ul*/
	this._fillUlWithLi = function ( data ) {
		
		$(_this.dom.li).remove()
		
		for( var id in data ){
			
			var dt = data[ id ];

			// create li element and append to ul
			var _li = $(this.templates.li).clone().appendTo(this.dom.ul);
			_li.find("input").attr({ id : this.name + "__" + dt.replace(/ /g,'' ), value : id });
			_li.find("label").attr({ 'for' : this.name + "__" + dt.replace(/ /g,'' ) }).text(dt);
			
			if( typeof( _this.selected[ id ] ) !== 'undefined' )
				_li.find("input").prop("checked", true);
		}
	};
	
	// construct data object
	this.data = new Core.Data( this.name, function ( data ) {
	
		var data = data.split(','),
			bunch_dict = {};
		
		// clear old values 
		$(_this.dom.checkboxes).prop("checked", false);
		_this.selected = {};
		
		//check checkboxes
		for ( var i in data ){
			
			// check in search_options data
			if( typeof( _this.search_options.data[ data[i] ] ) === 'undefined' )
				continue;
			
			var dt = _this.search_options.data[ data[i] ];
			
			// try to find checkbox and check it
			var el = $(_this.dom.checkboxes + "[value='"+ data[i] +"']");
			if( el.length != 0 ){
				el.prop("checked", true);
				
				// run depend functions
				for( var j in _this.dependCheckFnList ){
					_this.dependCheckFnList[j][0]( _this, _this.dependCheckFnList[j][1], el );
				}
				
			}
			// add values to bunch			
			bunch_dict[ data[i] ] = dt;
			
		}
		
		//display div if have some values
		if( Object.keys(bunch_dict).length > 0 && _this.depend )
			$(_this.depend_on.container_selector).show();
		
		// run extra function
		_this.afterChangeInput();
			
		_this.selected = bunch_dict;
		_this.bunchSet( bunch_dict );
		_this.changeInput();
		_this._reload_on_mouseleave = false;
	
	});
	
	/** Actions after change come value */
	this.afterChange = function ( reload ) {
		
		var bunch_dict = {},
			string = "";
		
		for( var i in this.selected ) {
			if( string.length > 0 )
				string += ",";
			bunch_dict[ i ] = this.selected[i]; 
			string += i;
		}
	
		this.bunchSet( bunch_dict );
		this.data.set( string, reload );
		
	};
	
	this.clear1 = function(){
		// run depend functions
		for( var i in this.dependCheckFnList ){
			this.dependCheckFnList[i][1].clear( false ); 
			$(this.dependCheckFnList[i][1].depend_on.container_selector).hide();
		}
		this.realClear( false ) ;
	};
	
	/** Remove one value and make after change */
	this.removeValue = function ( data ) {
		
		delete this.selected[ data ];
		
		$( this.dom.checkboxes + "[value='" + data +"']" ).prop("checked", false);

		// run depend functions
		for( var i in this.dependCheckFnList ){
			this.dependCheckFnList[i][1].clear( false ); 
			$(this.dependCheckFnList[i][1].depend_on.container_selector).hide();
		}
						
		this.changeInput();
		
		this.afterChange( true );
	}

	/** Changes after input change */
	this.changeInput = function ( ) {
		
		var ln = Object.keys(this.selected).length;
		
		switch( ln ){
			case 0:
				$(this.dom.input).val(this.null_input_value);
				break;
			case 1:
				for( var i in this.selected ){
					$(this.dom.input).val( this.selected[i] );
				}
				break;
			default:
				$(this.dom.input).val( ln + " Selected");
		}
	}
	
	// initialize current slider
	this.init()
}

SearchList.prototype = new Filter( );

/*
- Изначально выбрано пустое значение "-"
- По клику на add new отрисовывается форма добавления (инпут + сохранить)
        - После нажатия на сохранить на сервер посылается массив пользовательских фильтров в виде {"name": "URL"}
        - При mouseleave добавление отменяется, ставится значение "-"
- Если пользователь выбрал какой-то из фильтров, то в строку подставляется значение URL, идет релоад страницы с данным URL
- После загрузки страницы URL проверяется на равенство со всеми пользовательскими фильтрами и он выбирается в списке при совпадении
- После выбора значения из списка add заменяется на delete
- При выборе пустого значения снова показывается add
- С сервера пользовательские фильтры приходят в-виде {"name": "URL"}, как часть js кода при загрузки страницы.
*/


/** User filters data 
*/
Core.UserFiltersData = {
	
	// filter data storage
	filter_data : [],		
	
	// index of last added element
	index : 0,
	
	/** Add new filter data */
	add : function ( filter_name, filter_data ) {
			
		this.filter_data = this.filter_data || [];
		this.filter_data.push( [filter_name, filter_data] );
		
		//change internal index
		this.index = this.filter_data.length - 1;
	},
	
	/** Remove filter from list */
	remove : function ( filter_index ) {
		delete this.filter_data[ filter_index ];
		
		//change internal index
		this.index = this.filter_data.length - 1;
	},
	
	/** Find filter by string in data */
	indexOf : function ( filter_string ) {
		
		//	parse current url to data
		var filter_data = Core.URLparser.parseHash( filter_string );
		
		//	for each user filters data
		for ( var i in this.filter_data ){
			
			if (Core.UserFiltersData.filter_data[i] == null)
				continue;
			
			// parse current filter data
			var _filter_data = Core.URLparser.parseHash( this.filter_data[i][1] );
			
			// compare current data and filter data
			if( this._compareFilterDatas( filter_data, _filter_data ) )
				return i;
		}
		
		return false;
		
	},
	
	/** Compare two dict of datas */
	_compareFilterDatas : function ( data1, data2 ) {
		
		var correct_datas = 0,
			length = 0;

		// check for length
		if( Object.keys( data1 ).length != Object.keys( data2 ).length )
			return false;
		
		// check every elements
		for( var i in data1 ){
			length ++;
			
			for( var j in data2){
				if( i == j )
					if( data1[i][0] != data2[i][0] )
						return false;
					else 
						correct_datas ++;
			}
		}
		
		// check correct elements count
		if( correct_datas != length )
			return false;
			
		return true;
		
	}
};

/** User filters control 
*/
Views.UserFilters = {
	dom : { 
		div : ".user-filters",
		select : ".user-filters__select",
		options : ".user-filters__select option",
		selected : ".user-filters__select option:selected",
		add : ".user-filters__add",
		'delete' : ".user-filters__delete",
		'new' : ".user-filters__new",
			cancel : ".user-filters__cancel",
			save : ".user-filters__save",
			filter_name : ".user-filters__new input"
	},		

	/** Initial function */
	init : function () {
		
		var _this = this;
		
		Core.UserFiltersData.filter_data = $.parseJSON( localStorage.getItem('AWS-myFilters') );
		
		// clear and fill select with options
		$(this.dom.select).html('');
		this._createOptions();
		
		//	check current url for all filters
		var current_filter_index = Core.UserFiltersData.indexOf( window.location.hash.substr(1) );
		
		// if current_filter_index init it
		if( current_filter_index ){
			$(this.dom.options + "[value='" + current_filter_index + "']").prop("selected", true);
		}
		
		//	on click "add" show creation form
		$(this.dom.add).click( function() {
			
			$( _this.dom.div + " > div" ).hide();
			$( _this.dom['new'] ).show();
			
		});
		
		// on click "x" close form and clear
		$(this.dom.cancel).click( _this._onHideNew );
		
		//on click "save"
		$(this.dom.save).click( function () {
			_this._realSave()
		});
		
		// on click delete
		$(this.dom.delete).click( function (){
		
			// prevent delete default row
			if( $(_this.dom.selected).val().length == 0 )
				return;
				
			// remove from UserFiltersData
			Core.UserFiltersData.remove( $(_this.dom.selected).val() );
			
			// remove element
			$(_this.dom.selected).remove();
			
			// save user filter in the local storage
			Core.Sync.sendUserFilters();			

		} );
		
		// on key up
		$(document)
			.keyup( function( event ){
		
				// check for parent div visible
				if( ! $( _this.dom['new'] ).is(":visible") )
					return;
				
				// on esc
				if( event.which == 27 ){
					_this._onHideNew();
				}
				
				//on enter
				if( event.which == 13 ){
					_this._realSave();
				}
			}) 
			
		//  on change selector
		$(this.dom.select).change( function () {

			// hide delete for defalut option
			$(_this.dom.delete).show();
			
			if( $(_this.dom.selected).val().length == 0 ){
				$(_this.dom.delete).hide();
				return;
			}
			
			// get url from data
			var data = Core.UserFiltersData.filter_data[ $(_this.dom.selected).val() ];			
			
			//change URL
			window.location.hash = data[1];
			
			//reload page
			//location.reload();
			Core.update();
		});

	},

	/** real Save method **/
	_realSave : function (){		
		// add url # params row to user filters data
		var filter_data = window.location.hash.substr(1);
		var filter_name = $(this.dom.filter_name).val();
		
		Core.UserFiltersData.add( filter_name, filter_data );
		
		// send user filters data to server
		Core.Sync.sendUserFilters();
		
		// hide new
		this._onHideNew();
		
		// create option
		this._createOption({ value: Core.UserFiltersData.index, text : filter_name, selected : true })
	},
		
	/** Create options in select */
	_createOptions : function (){
		
		// create default
		this._createOption({ text : '  -', selected : true })
		
		// create from data
		for ( var i in Core.UserFiltersData.filter_data ){
			var cd = Core.UserFiltersData.filter_data[i];
			if (Core.UserFiltersData.filter_data[i] != null)
				this._createOption({ text : cd[0], value : i })
		}
		
	},
	
	/** Create options in select */
	_createOption : function ( attrs ){
		
		if( typeof( attrs['value'] ) === 'undefined' )
			attrs.value = "";
			
		if( typeof( attrs['selected'] ) === 'undefined' )
			attrs.selected = false;
			
		if( typeof( attrs['text'] ) === 'undefined' )
			attrs.text = "  -";	
			
		// add option to dropdown and select it
		$('<option></option>')
			.val( attrs.value )
			.text( attrs.text )
			.prop( "selected", attrs.selected )
			.appendTo( this.dom.select );
	},
			
	/** Actions on hide new div and show other */
	_onHideNew : function( event ){
		
		var _this = Views.UserFilters;
		
		// clear input
		$( _this.dom.filter_name ).val( "" );
		
		$( _this.dom.div + " > div" ).show();
		$( _this.dom.new ).hide();
	},
	
};


Views.UserFilters.init();

