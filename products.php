<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if( defined('DOING_AJAX') && DOING_AJAX ) {
	add_action('wp_ajax_nopriv_getServers', 'getServers_callback');
	add_action('wp_ajax_getServers', 'getServers_callback');
}

function getServers_callback () {	
	
	// parce incoming data
	$data = file_get_contents("php://input");
	$data = strstr($data, '#');
	$data = str_replace( "#", "&", $data);
	parse_str($data, $result);
	
	// get filters map
	$string = file_get_contents(dirname(__FILE__) . '/assets/js/filters_map.json');
	$filters_map = json_decode($string, true);
	
	// build sql
	$sql = '';
	
	foreach( $result as $i => $v ){
		// ":"
		if (strpos($v, ':') !== false) {
		    $values = explode( ":", $v );
			
			if( strlen( $sql ) > 0 )
				$sql .= " and ";
				
			$sql .= '('. $i .'>'. $values[0] .' and '. $i .'<'. $values[1] .')';
			
			continue;
		}
		
		// "," 
		if (strpos($v, ',') !== false) {
			$values = explode( ",", $v );
			
			if( strlen( $sql ) > 0 )
				$sql .= " and ";
				
			$_sql = "";

			foreach( $values as $val ){
				if( isset( $filters_map[$i] ) ){
					foreach( $filters_map[ $i ] as $fi => $fv ){
						if( $fv == $val ){
							if ( strlen( $_sql ) == 0 )
								$_sql .= ",";
							$_sql .= $fi;						
						}					
					}
				} else {
					if ( strlen( $_sql ) > 0 )
						$_sql .= ",";
					$_sql .= $val;
				}
			}
			
			$sql .= "(". $i ." in (". $_sql .") )";
			
			continue;
		}
		
		// single
		if( strlen( $sql ) > 0 )
			$sql .= " and " . $i . "=";			
		if( isset( $filters_map[$i] ) ) {
			foreach( $filters_map[ $i ] as $fi => $fv ){
				if( $fi == $v ){
					$sql .= $fv;
					break;
				}
			}
		} else {
			$sql .= $v;
		}
		
	}
	
	$sql = "select * from products where " . $sql;
	//echo $sql;

	$db = new SQLite3(dirname(__FILE__) . '/servers.db');
	
	$result = array();
	//$res = $db->query( $sql );	
	$res = $db->query("SELECT * FROM products LIMIT 10");
	while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
			
		//BRAND
		$ans = array( isset( $row['brands'] ) ? $row['brands'] : '' );
		
		// HOT DEAL
		$ans[] = '';
		
		// CPU
		$cpu_string = '';
		if( isset( $row['cpu_procs'] ) ) $cpu_string .= $row['cpu_procs'] . " x "; // 2 x
		if( isset( $row['cpu_brand'] ) ) $cpu_string .= $row['cpu_brand'] . " "; // Intel
		if( isset( $row['cpu_fam1'] ) ) $cpu_string .= $row['cpu_fam1'] . " "; // Xeon
		if( isset( $row['cpu_cores'] ) ) $cpu_string .= $row['cpu_cores'] . " Cores x "; // 8 Cores
		$ans[] = $cpu_string; 
		
		// CPU SCORE	
		$ans[] = isset( $row['cpu_score'] ) ? $row['cpu_score'] : '';
		
		// RAM
		$ram_string = isset( $row['ram_size'] ) ? $row['ram_size'] ."GB " : '';
		$ram_string .= isset( $row['ram_ecc'] ) ? $row['ram_ecc'] ."ECC" : '';
		$ans[] = $ram_string;
		
		// HDD
		$hdd_string = isset( $row['hdd_num'] ) ? $row['hdd_num'] ."x" : '';
		$hdd_string .= isset( $row['hdd_size'] ) ? $row['hdd_size'] ."GB" : '';
		$hdd_string .= isset( $row['hdd_type'] ) ? "-".$row['hdd_type'] : '';
		$ans[] = array($hdd_string);
		
		// TRAFFIC
		$traffic_string = isset( $row['bw_num'] ) ? $row['bw_num'] ." " : '';
		$traffic_string .= isset( $row['bw_uplink'] ) ? $row['bw_uplink'] : '';
		$ans[] = $traffic_string;
		
		//IP		
		$ans[] = array(
			isset( $row['address'] ) ? $row['address'] ." ipv4" : '' ,
			isset( $row['address2'] ) ? $row['address2'] ." ipv6" : ''
		);
				
		//LOCATION
		$ans[] = isset( $row['countries'] ) ? $row['countries'] : '';
		
		//PRICE
		$ans[] = isset( $row['server_cost'] ) ? $row['server_cost'] : '';
		
		$result[] = $ans;
	}

	print_r( json_encode( $result ) );
	wp_die();
}
/*
// TEST
$db = new SQLite3(dirname(__FILE__) . '/servers.db');

$res = $db->query("SELECT * FROM products LIMIT 1");
while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
	var_dump($row);
}

//
function ErrorReturn( $msg ){
	print_r( $msg );
	wp_die();	
}*/

?>